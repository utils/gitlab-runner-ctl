// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use gitlab::api::runners;

/// Filters available for runner queries
#[derive(Clone, Debug)]
pub enum RunnersFilter {
    /// Runner type (Instance/Group/Project)
    Type(runners::RunnerType),
    /// Runner status (Online/Offline/Stale/NeverContacted)
    Status(runners::RunnerStatus),
    /// Runner is paused
    Paused,
    /// Runner is not paused
    Running,
    /// Runner tags
    Tags(Vec<String>),
    /// Runner gitlab-runner version
    Version(String),
}

/// Trait for query builders to apply RunnersFilters
pub trait RunnersFilteredQuery {
    /// Apply the RunnersFilter to the query builder
    fn apply(&mut self, filter: RunnersFilter) -> &mut Self;
}

impl RunnersFilteredQuery for runners::RunnersBuilder<'_> {
    /// Apply RunnersFilter to RunnersBuilder
    fn apply(&mut self, filter: RunnersFilter) -> &mut Self {
        match filter {
            RunnersFilter::Type(t) => self.type_(t),
            RunnersFilter::Status(s) => self.status(s),
            RunnersFilter::Paused => self.paused(true),
            RunnersFilter::Running => self.paused(false),
            RunnersFilter::Tags(tags) => self.tags(tags.into_iter()),
            RunnersFilter::Version(v) => self.version_prefix(v),
        }
    }
}

impl RunnersFilteredQuery for runners::AllRunnersBuilder<'_> {
    /// Apply RunnersFilter to AllRunnersBuilder
    fn apply(&mut self, filter: RunnersFilter) -> &mut Self {
        match filter {
            RunnersFilter::Type(t) => self.type_(t),
            RunnersFilter::Status(s) => self.status(s),
            RunnersFilter::Paused => self.paused(true),
            RunnersFilter::Running => self.paused(false),
            RunnersFilter::Tags(tags) => self.tags(tags.into_iter()),
            RunnersFilter::Version(v) => self.version_prefix(v),
        }
    }
}

/// Filters available for runner job queries
#[derive(Clone, Debug)]
pub enum RunnerJobsFilter {
    /// Job Status (Runner/Success/Failed/Canceled)
    Status(runners::RunnerJobStatus),
}

/// Trait for query builders to apply RunnerJobsFilters
pub trait RunnerJobsFilteredQuery {
    /// Apply the RunnerJobssFilter to the query builder
    fn apply(&mut self, filter: RunnerJobsFilter) -> &mut Self;
}

impl RunnerJobsFilteredQuery for runners::RunnerJobsBuilder<'_> {
    /// Apply RunnerJobsFilter to RunnerJobsBuilder
    fn apply(&mut self, filter: RunnerJobsFilter) -> &mut Self {
        match filter {
            RunnerJobsFilter::Status(s) => self.status(s),
        }
    }
}
