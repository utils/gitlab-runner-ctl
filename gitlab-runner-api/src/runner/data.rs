// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, Utc};
use gitlab::api::runners;
use serde::Deserialize;

/// Pipeline information.
#[derive(Debug, Deserialize, Clone)]
pub struct Pipeline {
    /// The ID of the pipeline.
    pub id: u64,
    /// The URL of the pipeline.
    pub web_url: String,
}

/// Project information.
#[derive(Debug, Deserialize, Clone)]
pub struct Project {
    /// The ID of the project.
    pub id: u64,
    /// The name of the project.
    pub name: String,
    /// The path of the project on the instance.
    pub path_with_namespace: String,
}

/// Commit information.
#[derive(Debug, Deserialize, Clone)]
pub struct Commit {
    /// The full hash of the commit.
    pub id: String,
    /// The title of the commit message.
    pub title: String,
    /// The body of the commit message.
    pub message: String,
}

/// The status of a job on a runner.
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum RunnerJobStatus {
    /// The job is currently running.
    #[serde(rename = "running")]
    Running,
    /// The job has completed successfully.
    #[serde(rename = "success")]
    Success,
    /// The job has completed with failure.
    #[serde(rename = "failed")]
    Failed,
    /// The job was canceled before completion.
    #[serde(rename = "canceled")]
    Canceled,
}

/// Convertion from RunnerJobStatus JSON serializing enum to gitlab::api::runners::RunnerJobStatus enum
impl From<RunnerJobStatus> for runners::RunnerJobStatus {
    fn from(val: RunnerJobStatus) -> Self {
        match val {
            RunnerJobStatus::Running => runners::RunnerJobStatus::Running,
            RunnerJobStatus::Success => runners::RunnerJobStatus::Success,
            RunnerJobStatus::Failed => runners::RunnerJobStatus::Failed,
            RunnerJobStatus::Canceled => runners::RunnerJobStatus::Canceled,
        }
    }
}

/// The reasons a job can fail on a runner.
// See: app/models/concerns/enums/ci/commit_status.rb
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum FailureReason {
    /// The CI script failed.
    #[serde(rename = "script_failure")]
    ScriptFailure,
    /// Failed to communicate with GItLab.
    #[serde(rename = "api_failure")]
    ApiFailure,
    /// No runner accepted the job.
    #[serde(rename = "stuck_or_timeout_failure")]
    StuckOrTimeoutFailure,
    /// The runner itself had a failure.
    #[serde(rename = "runner_system_failure")]
    RunnerSystemFailure,
    /// Missing dependency failure.
    #[serde(rename = "missing_dependency_failure")]
    MissingDependencyFailure,
    /// Runner is out-of-date.
    #[serde(rename = "runner_unsupported")]
    RunnerUnsupported,
    /// A delayed job could not be executed.
    #[serde(rename = "stale_schedule")]
    StaleSchedule,
    /// The script exceeded the execute time limit.
    #[serde(rename = "job_execution_timeout")]
    JobExecutionTimeout,
    /// The job is archived.
    #[serde(rename = "archived_failure")]
    ArchivedFailure,
    /// A dependent job's artifacts have expired.
    #[serde(rename = "unmet_prerequisites")]
    UnmetPrerequisites,
    /// A runner could not be assigned the job.
    #[serde(rename = "scheduler_failure")]
    SchedulerFailure,
    /// Unknown job failure; see logs.
    #[serde(rename = "data_integrity_failure")]
    DataIntegrityFailure,
    /// The user that created the job is blocked.
    #[serde(rename = "user_blocked")]
    UserBlocked,
    /// The owning project has been deleted.
    #[serde(rename = "project_deleted")]
    ProjectDeleted,
    /// No more CI minutes available.
    #[serde(rename = "ci_quota_exceeded")]
    CiQuotaExceeded,
    /// The job would create a looping pipeline.
    #[serde(rename = "pipeline_loop_detected")]
    PipelineLoopDetected,
    /// No matching runner could be found.
    // No longer used.
    #[serde(rename = "no_matching_runner")]
    NoMatchingRunner,
    /// The job log size was reached.
    #[serde(rename = "trace_size_exceeded")]
    TraceSizeExceeded,
    /// CI is disabled for the project
    #[serde(rename = "builds_disabled")]
    BuildsDisabled,
    /// Invalid parameter for an environment.
    #[serde(rename = "environment_creation_failure")]
    EnvironmentCreationFailure,
    /// The deployment job was rejected.
    #[serde(rename = "deployment_rejected")]
    DeploymentRejected,
    /// The job would deploy over a newer deployment.
    #[serde(rename = "failed_outdated_deployment_job")]
    #[serde(alias = "forward_deployment_failure")] // Deprecated alias.
    FailedOutdatedDeploymentJob,
    /// Insufficient permissions to launch a protected job.
    #[serde(rename = "protected_environment_failure")]
    ProtectedEnvironmentFailure,
    /// Could not create a downstream pipeline due to insufficient permissions.
    #[serde(rename = "insufficient_bridge_permissions")]
    InsufficientBridgePermissions,
    /// The downstream bridge project could not be found.
    #[serde(rename = "downstream_bridge_project_not_found")]
    DownstreamBridgeProjectNotFound,
    /// The downstream pipeline trigger definition is invalid.
    #[serde(rename = "invalid_bridge_trigger")]
    InvalidBridgeTrigger,
    /// The upstream bridge project could not be found.
    #[serde(rename = "upstream_bridge_project_not_found")]
    UpstreamBridgeProjectNotFound,
    /// Insufficient permissions ot track the upstream project.
    #[serde(rename = "insufficient_upstream_permissions")]
    InsufficientUpstreamPermissions,
    /// Child pipelines cannot create their own child pipelines.
    #[serde(rename = "bridge_pipeline_is_child_pipeline")]
    BridgePipelineIsChildPipeline,
    /// The downstream pipeline could not be created.
    #[serde(rename = "downstream_pipeline_creation_failed")]
    DownstreamPipelineCreationFailed,
    /// The secrets provider could not be found.
    #[serde(rename = "secrets_provider_not_found")]
    SecretsProviderNotFound,
    /// The maximum depth of child piplines has been reached.
    #[serde(rename = "reached_max_descendant_pipelines_depth")]
    ReachedMaxDescendantPipelinesDepth,
    /// The runner's IP is outside of the allowed range.
    #[serde(rename = "ip_restriction_failure")]
    IpRestrictionFailure,
    /// The downstream pipeline is too large.
    #[serde(rename = "reached_max_pipeline_hierarchy_size")]
    ReachedMaxPipelineHierarchySize,
    /// Too many downstream pipelines within a window of time.
    #[serde(rename = "reached_downstream_pipeline_trigger_rate_limit")]
    ReachedDownstreamPipelineTriggerRateLimit,
}

/// Job information.
#[derive(Debug, Deserialize, Clone)]
pub struct Job {
    /// The ID fo the job.
    pub id: u64,

    // Pipeline metadata.
    /// The name of the stage within the pipeline.
    pub stage: String,
    /// The name of the job.
    pub name: String,
    /// The pipeline the job belongs to.
    pub pipeline: Pipeline,
    /// The URL of the job.
    pub web_url: String,

    // Git reference metadata.
    /// The name of the Git reference for the pipeline.
    #[serde(rename = "ref")]
    pub ref_: String,
    /// Whether the job is being built for a tag or not.
    pub tag: bool,
    /// The project which hosts the pipeline.
    pub project: Project,
    /// The commit for the pipeline.
    pub commit: Commit,

    // Runtime metadata.
    /// The status of the job.
    pub status: RunnerJobStatus,
    /// The runner for the job.
    pub runner: Option<RunnerDetails>,
    /// The reason for the failure (if any).
    pub failure_reason: Option<FailureReason>,
    /// When the job was created.
    pub created_at: DateTime<Utc>,
    /// When the job was started.
    pub started_at: Option<DateTime<Utc>>,
    /// When the job finished (if completed).
    pub finished_at: Option<DateTime<Utc>>,
    /// The duration of the job (in seconds).
    pub duration: Option<f64>,
    /// How long the job waited before it was started.
    pub queued_duration: f64,
}

/// An authentication token for a runner.
#[derive(Debug, Deserialize)]
pub struct RunnerToken {
    /// The content of the token.
    pub token: String,
    /// When the token expires.
    pub token_expires_at: Option<DateTime<Utc>>,
}

/// Types of runners.
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum RunnerType {
    /// Instance-wide runners.
    ///
    /// May perform tasks by any project.
    #[serde(rename = "instance_type")]
    Instance,
    /// Group-specific runners.
    ///
    /// May perform jobs by projects within this or any child group.
    #[serde(rename = "group_type")]
    Group,
    /// Project-specific runners.
    ///
    /// May only perform jobs owned by the project.
    #[serde(rename = "project_type")]
    Project,
}

/// Runner access permissions.
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum RunnerAccessLevel {
    /// Only perform jobs for pipelines associated with protected refs.
    #[serde(rename = "ref_protected")]
    RefProtected,
    /// Perform jobs for any eligible pipeline.
    #[serde(rename = "not_protected")]
    NotProtected,
}

/// Overview of a runner.
#[derive(Debug, Clone, Deserialize)]
pub struct RunnerOverview {
    /// The ID of the runner.
    pub id: u64,

    // Metadata.
    /// The name of the runner.
    pub name: Option<String>,
    /// The description of the runner.
    pub description: Option<String>,

    // Registration information.
    /// Whether the runner is shared or not.
    pub is_shared: bool,
    /// The type of the runner.
    pub runner_type: RunnerType,

    // State information.
    /// Whether the runner is paused or not.
    pub paused: bool,
    /// Whether the runner is online or not.
    pub online: Option<bool>,
}

impl RunnerOverview {
    /// Upgrade an overview into a (partial) `RunnerDetails` structure.
    ///
    /// Fields will be incomplete, so only do this if the fields not present in `RunnerOverview`
    /// are not required.
    pub fn upgrade(self) -> RunnerDetails {
        RunnerDetails {
            id: self.id,
            name: self.name,
            description: self.description,
            maintenance_note: None,
            is_shared: self.is_shared,
            runner_type: self.runner_type,
            tags: Vec::new(),
            paused: self.paused,
            online: self.online,
            contacted_at: None,
            access_level: RunnerAccessLevel::RefProtected,
            maximum_timeout: None,
            run_untagged: None,
        }
    }
}

/// Details on a runner.
#[derive(Debug, Clone, Deserialize)]
pub struct RunnerDetails {
    /// The ID of the runner.
    pub id: u64,

    // Metadata.
    /// The name of the runner.
    pub name: Option<String>,
    /// The description of the runner.
    pub description: Option<String>,
    /// Maintenance note of the runner.
    pub maintenance_note: Option<String>,

    // Registration information.
    /// Whether the runner is shared or not.
    pub is_shared: bool,
    /// The type of the runner.
    pub runner_type: RunnerType,
    /// The set of tags for the runner.
    #[serde(default, rename = "tag_list")]
    pub tags: Vec<String>,

    // State information.
    /// Whether the runner is paused or not.
    pub paused: bool,
    /// Whether the runner is online or not.
    pub online: Option<bool>,
    /// When the runner last contacted the GitLab instance.
    pub contacted_at: Option<DateTime<Utc>>,

    // Settings.
    /// The access level to the runner.
    pub access_level: RunnerAccessLevel,
    /// The maximum timeout allowed for the runner.
    pub maximum_timeout: Option<u64>,
    /// Whether the runner requires tagged jobs or not.
    pub run_untagged: Option<bool>,
}
