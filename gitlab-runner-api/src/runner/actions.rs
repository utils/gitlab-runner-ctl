// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Runner actions
//!
//! This module provides functions which may be used to edit settings on a runner.

use std::borrow::Cow;

use gitlab::api::runners::EditRunnerBuilder;

/// Pause a runner.
pub fn pause(builder: &mut EditRunnerBuilder) {
    builder.paused(true);
}

/// Unpause a runner.
pub fn unpause(builder: &mut EditRunnerBuilder) {
    builder.paused(false);
}

/// Lock a runner to its current set of projects.
pub fn lock(builder: &mut EditRunnerBuilder) {
    builder.locked(true);
}

/// Unlock a runner for use by other projects.
pub fn unlock(builder: &mut EditRunnerBuilder) {
    builder.locked(false);
}

/// Add a description to a runner.
pub fn describe<'a, D>(description: D) -> impl FnOnce(&mut EditRunnerBuilder<'a>) + 'a
where
    D: Into<Cow<'a, str>> + 'a,
{
    move |builder: &mut EditRunnerBuilder| {
        builder.description(description.into());
    }
}

/// Set the tags for a runner.
pub fn set_tags<'a, I, T>(tags: I) -> impl FnOnce(&mut EditRunnerBuilder<'a>) + 'a
where
    I: Iterator<Item = T> + 'a,
    T: Into<Cow<'a, str>> + 'a,
{
    move |builder: &mut EditRunnerBuilder| {
        builder.tags(tags);
    }
}

/// Whether the runner can accept untagged jobs or not.
pub fn untagged_jobs(untagged: bool) -> impl FnOnce(&mut EditRunnerBuilder) {
    move |builder: &mut EditRunnerBuilder| {
        builder.run_untagged(untagged);
    }
}
