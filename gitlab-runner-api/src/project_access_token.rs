// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::collections::BTreeSet;

use chrono::NaiveDate;
use derive_builder::Builder;
use futures_util::{Stream, StreamExt};
use gitlab::api::common::NameOrId;
use gitlab::api::projects::access_tokens::{
    CreateProjectAccessTokenBuilder, ProjectAccessTokenAccessLevel, ProjectAccessTokenScope,
};
use gitlab::api::{self, AsyncQuery};
use serde::Deserialize;

pub mod data;

use self::data::ProjectAccessTokenDetails;

/// Parameters when creating a personal access token.
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct ProjectAccessTokenParameters<'a> {
    /// The name of the user.
    #[builder(setter(into))]
    name: Cow<'a, str>,
    /// The scopes the personal access token can use.
    #[builder(setter(name = "_scopes"), private)]
    scopes: BTreeSet<ProjectAccessTokenScope>,
    /// The access level of the token to the project.
    #[builder(default)]
    access_level: Option<ProjectAccessTokenAccessLevel>,
    /// When the personal access token expires.
    #[builder(default)]
    expires_at: Option<NaiveDate>,
}

impl<'a> ProjectAccessTokenParameters<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> ProjectAccessTokenParametersBuilder<'a> {
        ProjectAccessTokenParametersBuilder::default()
    }

    fn build(self, builder: &mut CreateProjectAccessTokenBuilder<'a>) {
        builder.name(self.name).scopes(self.scopes.into_iter());
        if let Some(access_level) = self.access_level {
            builder.access_level(access_level);
        }
        if let Some(expires_at) = self.expires_at {
            builder.expires_at(expires_at);
        }
    }
}

impl ProjectAccessTokenParametersBuilder<'_> {
    /// Add a scope for the token.
    pub fn scope(&mut self, scope: ProjectAccessTokenScope) -> &mut Self {
        self.scopes.get_or_insert_with(BTreeSet::new).insert(scope);
        self
    }

    /// Add scopes for the token.
    pub fn scopes<I>(&mut self, scopes: I) -> &mut Self
    where
        I: Iterator<Item = ProjectAccessTokenScope>,
    {
        self.scopes.get_or_insert_with(BTreeSet::new).extend(scopes);
        self
    }
}

/// A personal access token on a given client.
#[derive(Debug)]
pub struct ProjectAccessToken<'a, C> {
    client: C,

    /// The project the token belongs to.
    pub project: NameOrId<'a>,
    /// The ID of the project access token.
    pub id: u64,
}

#[derive(Debug, Deserialize)]
pub struct NewProjectAccessToken {
    /// Details about the token.
    #[serde(flatten)]
    pub details: ProjectAccessTokenDetails,
    /// The token.
    pub token: String,
}

impl<'a, C> ProjectAccessToken<'a, C>
where
    C: api::AsyncClient + Sync,
{
    /// Create a personal access token given an ID.
    pub async fn by_id(
        client: C,
        project: NameOrId<'a>,
        id: u64,
    ) -> Result<(Self, ProjectAccessTokenDetails), api::ApiError<C::Error>> {
        let endpoint = api::projects::access_tokens::ProjectAccessToken::builder()
            .project(project.clone())
            .id(id)
            .build()
            .unwrap();
        let new_pat: ProjectAccessTokenDetails = endpoint.query_async(&client).await?;
        let pat = Self {
            client,
            project,
            id: new_pat.id,
        };
        Ok((pat, new_pat))
    }

    /// Create a new personal access token for a project.
    pub async fn create(
        client: C,
        project: NameOrId<'a>,
        parameters: ProjectAccessTokenParameters<'_>,
    ) -> Result<(Self, NewProjectAccessToken), api::ApiError<C::Error>> {
        let mut builder = api::projects::access_tokens::CreateProjectAccessToken::builder();
        builder.project(project.clone());
        parameters.build(&mut builder);
        let endpoint = builder.build().unwrap();
        let new_pat: NewProjectAccessToken = endpoint.query_async(&client).await?;
        let pat = Self {
            client,
            project,
            id: new_pat.details.id,
        };
        Ok((pat, new_pat))
    }

    /// Fetch details for the personal access token.
    pub async fn details(&self) -> Result<ProjectAccessTokenDetails, api::ApiError<C::Error>> {
        let endpoint = api::projects::access_tokens::ProjectAccessToken::builder()
            .project(self.project.clone())
            .id(self.id)
            .build()
            .unwrap();
        endpoint.query_async(&self.client).await
    }

    /// Rotate the personal access token.
    pub async fn rotate<E>(
        self,
        expires_at: E,
    ) -> Result<(Self, NewProjectAccessToken), api::ApiError<C::Error>>
    where
        E: Into<Option<NaiveDate>>,
    {
        let mut builder = api::projects::access_tokens::RotateProjectAccessToken::builder();
        builder.project(self.project.clone());
        builder.id(self.id);
        if let Some(expires_at) = expires_at.into() {
            builder.expires_at(expires_at);
        }
        let endpoint = builder.build().unwrap();
        let new_pat: NewProjectAccessToken = endpoint.query_async(&self.client).await?;
        let pat = Self {
            client: self.client,
            project: self.project,
            id: new_pat.details.id,
        };
        Ok((pat, new_pat))
    }

    /// Revoke the token.
    pub async fn revoke(self) -> Result<(), api::ApiError<C::Error>> {
        let endpoint = api::projects::access_tokens::RevokeProjectAccessToken::builder()
            .project(self.project)
            .id(self.id)
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        endpoint.query_async(&self.client).await
    }
}

impl<'a, C> ProjectAccessToken<'a, C>
where
    C: api::AsyncClient + Clone + Sync,
{
    fn project_access_token_response(
        client: &C,
        project: NameOrId<'a>,
        response: Result<ProjectAccessTokenDetails, api::ApiError<C::Error>>,
    ) -> Result<(ProjectAccessToken<'a, C>, ProjectAccessTokenDetails), api::ApiError<C::Error>>
    {
        response.map(|details| {
            (
                ProjectAccessToken {
                    client: client.clone(),
                    project,
                    id: details.id,
                },
                details,
            )
        })
    }

    /// List personal access tokens on an instance visible to the authenticated user.
    pub fn list(
        client: &'a C,
        project: NameOrId<'a>,
    ) -> impl Stream<
        Item = Result<
            (ProjectAccessToken<'a, C>, ProjectAccessTokenDetails),
            api::ApiError<C::Error>,
        >,
    > + 'a {
        let endpoint = api::projects::access_tokens::ProjectAccessTokens::builder()
            .project(project.clone())
            .build()
            .unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::project_access_token_response(client, project.clone(), rsp))
    }
}
