// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::num::NonZeroUsize;

use futures_util::{Stream, StreamExt};
use gitlab::api::{self, AsyncQuery};
use serde::Deserialize;

pub mod actions;
pub mod data;
pub mod filters;

use self::data::{Job, RunnerDetails, RunnerOverview, RunnerToken};
use self::filters::{
    RunnerJobsFilter, RunnerJobsFilteredQuery, RunnersFilter, RunnersFilteredQuery,
};

/// A runner on a given client.
#[derive(Debug, Clone)]
pub struct Runner<C> {
    client: C,

    /// The ID of the runner.
    pub id: u64,
}

/// Response when registering a new runner.
#[derive(Debug, Deserialize)]
struct NewRunner {
    /// The ID of the new runner.
    pub id: u64,
    /// The authentication token for the runner.
    #[serde(flatten)]
    pub token: RunnerToken,
}

impl<C> Runner<C>
where
    C: api::AsyncClient + Sync,
{
    /// Register a new runner through through a client.
    ///
    /// Takes a client to register against. Also accepts a function to set options.
    pub async fn register<B>(
        client: C,
        build: B,
    ) -> Result<(Self, RunnerToken), api::ApiError<C::Error>>
    where
        B: FnOnce(&mut api::users::CreateRunnerBuilder),
    {
        let mut builder = api::users::CreateRunner::builder();
        build(&mut builder);
        let endpoint = builder.build().unwrap();
        let new_runner: NewRunner = endpoint.query_async(&client).await?;
        let runner = Self {
            client,
            id: new_runner.id,
        };
        Ok((runner, new_runner.token))
    }

    /// Fetch details for the runner.
    pub async fn details(&self) -> Result<RunnerDetails, api::ApiError<C::Error>> {
        let endpoint = api::runners::Runner::builder()
            .runner(self.id)
            .build()
            .unwrap();
        endpoint.query_async(&self.client).await
    }

    /// Modify a runner.
    ///
    /// Accepts a function to set options to change.
    pub async fn update<E>(&self, edit: E) -> Result<RunnerDetails, api::ApiError<C::Error>>
    where
        E: FnOnce(&mut api::runners::EditRunnerBuilder),
    {
        let mut builder = api::runners::EditRunner::builder();
        edit(&mut builder);
        let endpoint = builder.runner(self.id).build().unwrap();
        endpoint.query_async(&self.client).await
    }

    /// Query the set of jobs for the runner.
    pub fn all_jobs(&self) -> impl Stream<Item = Result<Job, api::ApiError<C::Error>>> + '_ {
        let endpoint = api::runners::RunnerJobs::builder()
            .runner(self.id)
            .build()
            .unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint.into_iter_async(&self.client)
    }

    /// Query a subset of jobs for this runner.
    pub fn jobs(
        &self,
        count: Option<NonZeroUsize>,
        filters: Vec<Option<RunnerJobsFilter>>,
    ) -> impl Stream<Item = Result<Job, api::ApiError<C::Error>>> + '_ {
        let mut builder = api::runners::RunnerJobs::builder();

        // Apply all of the job filters
        for filter in filters.into_iter().flatten() {
            builder.apply(filter);
        }

        // Set the job fetch limit
        let limit = if let Some(n) = count {
            api::Pagination::Limit(n.get())
        } else {
            api::Pagination::All
        };

        let endpoint = builder
            .runner(self.id)
            .order_by(api::runners::RunnerJobsOrderBy::Id)
            .sort(api::common::SortOrder::Descending)
            .build()
            .unwrap();
        let endpoint = api::paged(endpoint, limit);
        endpoint.into_iter_async(&self.client)
    }

    /// Reset the authentication token for the runner.
    pub async fn reset_authentication_token(
        &mut self,
    ) -> Result<RunnerToken, api::ApiError<C::Error>> {
        let endpoint = api::runners::ResetRunnerAuthenticationToken::builder()
            .runner(self.id)
            .build()
            .unwrap();
        endpoint.query_async(&self.client).await
    }

    /// Delete the runner.
    pub async fn delete(self) -> Result<(), api::ApiError<C::Error>> {
        let endpoint = api::runners::DeleteRunner::builder()
            .runner(self.id)
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        endpoint.query_async(&self.client).await
    }
}

impl<C> Runner<C>
where
    C: api::AsyncClient + Clone + Sync,
{
    fn runner_response(
        client: &C,
        response: Result<RunnerOverview, api::ApiError<C::Error>>,
    ) -> Result<(Runner<C>, RunnerOverview), api::ApiError<C::Error>> {
        response.map(|overview| {
            (
                Runner {
                    client: client.clone(),
                    id: overview.id,
                },
                overview,
            )
        })
    }

    /// List all runners on an instance.
    ///
    /// Requires administrator privileges.
    pub fn list_all(
        client: &C,
    ) -> impl Stream<Item = Result<(Runner<C>, RunnerOverview), api::ApiError<C::Error>>> + '_ {
        let endpoint = api::runners::AllRunners::builder().build().unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::runner_response(client, rsp))
    }

    /// List runners on an instance visible to a user.
    pub fn list(
        client: &C,
    ) -> impl Stream<Item = Result<(Runner<C>, RunnerOverview), api::ApiError<C::Error>>> + '_ {
        let endpoint = api::runners::Runners::builder().build().unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::runner_response(client, rsp))
    }

    /// List a server side filtered subset of runners.
    pub fn query_all<F>(
        client: &C,
        filters: F,
    ) -> impl Stream<Item = Result<(Runner<C>, RunnerOverview), api::ApiError<C::Error>>> + '_
    where
        F: IntoIterator<Item = RunnersFilter>,
    {
        let mut builder = api::runners::AllRunners::builder();

        for filter in filters.into_iter() {
            builder.apply(filter);
        }

        let endpoint = builder.build().unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::runner_response(client, rsp))
    }

    /// List a server side filtered subset of runners.
    pub fn query<F>(
        client: &C,
        filters: F,
    ) -> impl Stream<Item = Result<(Runner<C>, RunnerOverview), api::ApiError<C::Error>>> + '_
    where
        F: IntoIterator<Item = RunnersFilter>,
    {
        let mut builder = api::runners::Runners::builder();

        for filter in filters.into_iter() {
            builder.apply(filter);
        }

        let endpoint = builder.build().unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::runner_response(client, rsp))
    }
}
