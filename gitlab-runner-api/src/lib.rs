// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![deny(missing_docs)]

//! GitLab Runner API
//!
//! This crate provides abstractions to query and manage runners on a GitLab instance.

mod personal_access_token;
mod project_access_token;
mod runner;

/// The `gitlab` crate used.
pub use gitlab;

// Actions
pub use runner::actions;

// Data
pub use personal_access_token::data::PersonalAccessTokenDetails;
pub use personal_access_token::data::PersonalAccessTokenScope;
pub use project_access_token::data::ProjectAccessTokenDetails;
pub use project_access_token::data::ProjectAccessTokenScope;
pub use runner::data::Commit;
pub use runner::data::Job;
pub use runner::data::Pipeline;
pub use runner::data::Project;
pub use runner::data::RunnerAccessLevel;
pub use runner::data::RunnerDetails;
pub use runner::data::RunnerJobStatus;
pub use runner::data::RunnerOverview;
pub use runner::data::RunnerToken;
pub use runner::data::RunnerType;

// Filters
pub use personal_access_token::filters::PersonalAccessTokenState;
pub use personal_access_token::filters::PersonalAccessTokensFilter;
pub use runner::filters::RunnerJobsFilter;
pub use runner::filters::RunnersFilter;

// Runner
pub use runner::Runner;

// Personal access tokens
pub use personal_access_token::PersonalAccessToken;
pub use personal_access_token::PersonalAccessTokenParameters;
pub use personal_access_token::PersonalAccessTokenParametersBuilder;
pub use personal_access_token::PersonalAccessTokenParametersBuilderError;

// Project access tokens
pub use project_access_token::ProjectAccessToken;
pub use project_access_token::ProjectAccessTokenParameters;
pub use project_access_token::ProjectAccessTokenParametersBuilder;
pub use project_access_token::ProjectAccessTokenParametersBuilderError;
