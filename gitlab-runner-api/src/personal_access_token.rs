// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::collections::BTreeSet;

use chrono::NaiveDate;
use derive_builder::Builder;
use futures_util::{Stream, StreamExt};
use gitlab::api::users::personal_access_tokens::{
    CreatePersonalAccessTokenBuilder, CreatePersonalAccessTokenForUserBuilder,
    PersonalAccessTokenCreateScope, PersonalAccessTokenScope,
};
use gitlab::api::{self, AsyncQuery};
use serde::Deserialize;

pub mod data;
pub mod filters;

use self::data::PersonalAccessTokenDetails;
use self::filters::{PersonalAccessTokensFilter, PersonalAccessTokensFilteredQuery};

/// Parameters when creating a personal access token.
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct PersonalAccessTokenParameters<'a> {
    /// The name of the user.
    #[builder(setter(into))]
    name: Cow<'a, str>,
    /// The scopes the personal access token can use.
    #[builder(setter(name = "_scopes"), private)]
    scopes: BTreeSet<PersonalAccessTokenScope>,
    /// The scopes the personal access token can use.
    #[builder(private, default)]
    current_scopes: BTreeSet<PersonalAccessTokenCreateScope>,
    /// When the personal access token expires.
    #[builder(default)]
    expires_at: Option<NaiveDate>,
}

impl<'a> PersonalAccessTokenParameters<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> PersonalAccessTokenParametersBuilder<'a> {
        PersonalAccessTokenParametersBuilder::default()
    }

    fn build_current(self, builder: &mut CreatePersonalAccessTokenBuilder<'a>) {
        builder
            .name(self.name)
            .scopes(self.current_scopes.into_iter());
        if let Some(expires_at) = self.expires_at {
            builder.expires_at(expires_at);
        }
    }

    fn build_for_user(self, builder: &mut CreatePersonalAccessTokenForUserBuilder<'a>) {
        builder.name(self.name).scopes(self.scopes.into_iter());
        if let Some(expires_at) = self.expires_at {
            builder.expires_at(expires_at);
        }
    }
}

impl PersonalAccessTokenParametersBuilder<'_> {
    /// Add a scope for the token.
    pub fn scope(&mut self, scope: PersonalAccessTokenScope) -> &mut Self {
        self.scopes.get_or_insert_with(BTreeSet::new).insert(scope);
        if let Some(create_scope) = scope.as_create_scope() {
            self.current_scopes
                .get_or_insert_with(BTreeSet::new)
                .insert(create_scope);
        }
        self
    }

    /// Add scopes for the token.
    pub fn scopes<I>(&mut self, scopes: I) -> &mut Self
    where
        I: Iterator<Item = PersonalAccessTokenScope>,
    {
        scopes.for_each(|s| {
            self.scope(s);
        });
        self
    }
}

/// A personal access token on a given client.
#[derive(Debug)]
pub struct PersonalAccessToken<C> {
    client: C,

    /// The ID of the personal access token.
    pub id: u64,
}

#[derive(Debug, Deserialize)]
pub struct NewPersonalAccessToken {
    /// Details about the token.
    #[serde(flatten)]
    pub details: PersonalAccessTokenDetails,
    /// The token.
    pub token: String,
}

impl<C> PersonalAccessToken<C>
where
    C: api::AsyncClient + Sync,
{
    /// Create a personal access token given an ID.
    pub async fn by_id(
        client: C,
        id: u64,
    ) -> Result<(Self, PersonalAccessTokenDetails), api::ApiError<C::Error>> {
        let endpoint = api::personal_access_tokens::PersonalAccessToken::builder()
            .id(id)
            .build()
            .unwrap();
        let new_pat: PersonalAccessTokenDetails = endpoint.query_async(&client).await?;
        let pat = Self {
            client,
            id: new_pat.id,
        };
        Ok((pat, new_pat))
    }

    /// Create a new personal access token for the authenticated user.
    pub async fn create(
        client: C,
        parameters: PersonalAccessTokenParameters<'_>,
    ) -> Result<(Self, NewPersonalAccessToken), api::ApiError<C::Error>> {
        let mut builder = api::users::personal_access_tokens::CreatePersonalAccessToken::builder();
        parameters.build_current(&mut builder);
        let endpoint = builder.build().unwrap();
        let new_pat: NewPersonalAccessToken = endpoint.query_async(&client).await?;
        let pat = Self {
            client,
            id: new_pat.details.id,
        };
        Ok((pat, new_pat))
    }

    /// Create a new personal access token for a given user.
    pub async fn create_for(
        client: C,
        for_user: u64,
        parameters: PersonalAccessTokenParameters<'_>,
    ) -> Result<(Self, NewPersonalAccessToken), api::ApiError<C::Error>> {
        let mut builder =
            api::users::personal_access_tokens::CreatePersonalAccessTokenForUser::builder();
        builder.user(for_user);
        parameters.build_for_user(&mut builder);
        let endpoint = builder.build().unwrap();
        let new_pat: NewPersonalAccessToken = endpoint.query_async(&client).await?;
        let pat = Self {
            client,
            id: new_pat.details.id,
        };
        Ok((pat, new_pat))
    }

    /// Fetch details for the personal access token.
    pub async fn details(&self) -> Result<PersonalAccessTokenDetails, api::ApiError<C::Error>> {
        let endpoint = api::personal_access_tokens::PersonalAccessToken::builder()
            .id(self.id)
            .build()
            .unwrap();
        endpoint.query_async(&self.client).await
    }

    /// Rotate the personal access token.
    pub async fn rotate<E>(
        self,
        expires_at: E,
    ) -> Result<(Self, NewPersonalAccessToken), api::ApiError<C::Error>>
    where
        E: Into<Option<NaiveDate>>,
    {
        let mut builder = api::personal_access_tokens::RotatePersonalAccessToken::builder();
        builder.id(self.id);
        if let Some(expires_at) = expires_at.into() {
            builder.expires_at(expires_at);
        }
        let endpoint = builder.build().unwrap();
        let new_pat: NewPersonalAccessToken = endpoint.query_async(&self.client).await?;
        let pat = Self {
            client: self.client,
            id: new_pat.details.id,
        };
        Ok((pat, new_pat))
    }

    /// Rotate the personal access token used for a client.
    ///
    /// Consumes the client as its token becomes unusable after this call.
    pub async fn rotate_client<E>(
        client: C,
        expires_at: E,
    ) -> Result<NewPersonalAccessToken, api::ApiError<C::Error>>
    where
        E: Into<Option<NaiveDate>>,
    {
        let mut builder = api::personal_access_tokens::RotatePersonalAccessTokenSelf::builder();
        if let Some(expires_at) = expires_at.into() {
            builder.expires_at(expires_at);
        }
        let endpoint = builder.build().unwrap();
        endpoint.query_async(&client).await
    }

    /// Revoke the token.
    pub async fn revoke(self) -> Result<(), api::ApiError<C::Error>> {
        let endpoint = api::personal_access_tokens::RevokePersonalAccessToken::builder()
            .id(self.id)
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        endpoint.query_async(&self.client).await
    }

    /// Revoke the token used for a client.
    ///
    /// Consumes the client as its token becomes unusable after this call.
    pub async fn revoke_client(client: C) -> Result<(), api::ApiError<C::Error>> {
        let endpoint = api::personal_access_tokens::RevokePersonalAccessTokenSelf::builder()
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        endpoint.query_async(&client).await
    }
}

impl<C> PersonalAccessToken<C>
where
    C: api::AsyncClient + Clone + Sync,
{
    fn personal_access_token_response(
        client: &C,
        response: Result<PersonalAccessTokenDetails, api::ApiError<C::Error>>,
    ) -> Result<(PersonalAccessToken<C>, PersonalAccessTokenDetails), api::ApiError<C::Error>> {
        response.map(|details| {
            (
                PersonalAccessToken {
                    client: client.clone(),
                    id: details.id,
                },
                details,
            )
        })
    }

    /// List personal access tokens on an instance visible to the authenticated user.
    pub fn list(
        client: &C,
    ) -> impl Stream<
        Item = Result<
            (PersonalAccessToken<C>, PersonalAccessTokenDetails),
            api::ApiError<C::Error>,
        >,
    > + '_ {
        let endpoint = api::personal_access_tokens::PersonalAccessTokens::builder()
            .build()
            .unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::personal_access_token_response(client, rsp))
    }

    /// List personal access tokens on an instance visible to the authenticated user.
    ///
    /// This method uses server-side filtering to avoid fetching unnecessary information.
    pub fn query<F>(
        client: &C,
        filters: F,
    ) -> impl Stream<
        Item = Result<
            (PersonalAccessToken<C>, PersonalAccessTokenDetails),
            api::ApiError<C::Error>,
        >,
    > + '_
    where
        F: IntoIterator<Item = PersonalAccessTokensFilter>,
    {
        let mut builder = api::personal_access_tokens::PersonalAccessTokens::builder();

        for filter in filters.into_iter() {
            builder.apply(filter);
        }

        let endpoint = builder.build().unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        endpoint
            .into_iter_async(client)
            .map(move |rsp| Self::personal_access_token_response(client, rsp))
    }
}
