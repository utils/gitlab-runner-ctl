// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, Utc};
use gitlab::api::personal_access_tokens;
use serde::Deserialize;

/// Scopes for personal access tokens.
#[derive(Debug, Deserialize, Clone, Copy)]
pub enum PersonalAccessTokenState {
    /// Tokens which are active.
    #[serde(rename = "active")]
    Active,
    /// Tokens which are inactive.
    #[serde(rename = "inactive")]
    Inactive,
}

impl PersonalAccessTokenState {
    /// Invert the state query.
    pub fn invert(self) -> Self {
        match self {
            Self::Active => Self::Inactive,
            Self::Inactive => Self::Active,
        }
    }
}

impl From<PersonalAccessTokenState> for personal_access_tokens::PersonalAccessTokenState {
    fn from(val: PersonalAccessTokenState) -> Self {
        match val {
            PersonalAccessTokenState::Active => Self::Active,
            PersonalAccessTokenState::Inactive => Self::Inactive,
        }
    }
}

/// Filters available for runner queries
#[derive(Clone, Debug)]
pub enum PersonalAccessTokensFilter {
    /// Tokens created after a time.
    CreatedAfter(DateTime<Utc>),
    /// Tokens created before a time.
    CreatedBefore(DateTime<Utc>),
    /// Tokens last used after a time.
    LastUsedAfter(DateTime<Utc>),
    /// Tokens last used before a time.
    LastUsedBefore(DateTime<Utc>),
    /// Tokens with a given revocation status.
    Revoked(bool),
    /// Tokens with a name matching a search string.
    Search(String),
    /// Tokens with a given state.
    State(PersonalAccessTokenState),
    /// Tokens owned by a given user by ID.
    UserId(u64),
    /// Tokens owned by a given user.
    Username(String),
}

/// Trait for query builders to apply PersonalAccessTokensFilter
pub trait PersonalAccessTokensFilteredQuery {
    /// Apply the PersonalAccessTokensFilter to the query builder
    fn apply(&mut self, filter: PersonalAccessTokensFilter) -> &mut Self;
}

impl PersonalAccessTokensFilteredQuery for personal_access_tokens::PersonalAccessTokensBuilder<'_> {
    /// Apply PersonalAccessTokensFilter to PersonalAccessTokensBuilder
    fn apply(&mut self, filter: PersonalAccessTokensFilter) -> &mut Self {
        match filter {
            PersonalAccessTokensFilter::CreatedAfter(t) => self.created_after(t),
            PersonalAccessTokensFilter::CreatedBefore(t) => self.created_before(t),
            PersonalAccessTokensFilter::LastUsedAfter(t) => self.last_used_after(t),
            PersonalAccessTokensFilter::LastUsedBefore(t) => self.last_used_before(t),
            PersonalAccessTokensFilter::Revoked(r) => self.revoked(r),
            PersonalAccessTokensFilter::Search(s) => self.search(s),
            PersonalAccessTokensFilter::State(s) => self.state(s.into()),
            PersonalAccessTokensFilter::UserId(u) => self.user(u),
            PersonalAccessTokensFilter::Username(u) => self.user(u),
        }
    }
}
