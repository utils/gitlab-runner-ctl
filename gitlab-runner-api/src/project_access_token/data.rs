// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, NaiveDate, Utc};
use serde::Deserialize;

/// Scopes for project access tokens.
#[derive(Debug, Deserialize, Clone, Copy)]
pub enum ProjectAccessTokenScope {
    /// Access the API and perform git reads and writes.
    #[serde(rename = "api")]
    Api,
    /// Access read-only API endpoints.
    #[serde(rename = "read_api")]
    ReadApi,
    /// Read access to repositories.
    #[serde(rename = "read_repository")]
    ReadRepository,
    /// Write access to repositories.
    #[serde(rename = "write_repository")]
    WriteRepository,
    /// Read access to Docker registries.
    #[serde(rename = "read_registry")]
    ReadRegistry,
    /// Write access to Docker registries.
    #[serde(rename = "write_registry")]
    WriteRegistry,
    /// Permission to create instance runners.
    #[serde(rename = "create_runner")]
    CreateRunner,
    /// Access to AI features (GitLab Duo for JetBrains).
    #[serde(rename = "ai_features")]
    AiFeatures,
    /// Access to perform Kubernetes API calls.
    #[serde(rename = "k8s_proxy")]
    K8sProxy,
}

impl ProjectAccessTokenScope {
    /// A string for the scope.
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Api => "api",
            Self::ReadApi => "read_api",
            Self::ReadRepository => "read_repository",
            Self::WriteRepository => "write_repository",
            Self::ReadRegistry => "read_registry",
            Self::WriteRegistry => "write_registry",
            Self::CreateRunner => "create_runner",
            Self::AiFeatures => "ai_features",
            Self::K8sProxy => "k8s_proxy",
        }
    }
}

/// Details for a project access token.
#[derive(Debug, Deserialize)]
pub struct ProjectAccessTokenDetails {
    /// The ID of the project access token.
    pub id: u64,
    /// The name of the project access token.
    pub name: String,
    /// Whether the project access token has been revoked or not.
    pub revoked: bool,
    /// When the project access token was created.
    pub created_at: DateTime<Utc>,
    /// The scopes the project access token has access to.
    pub scopes: Vec<ProjectAccessTokenScope>,
    /// The user ID that owns the project access token.
    pub user_id: u64,
    /// When the project access token was last used.
    pub last_used_at: Option<DateTime<Utc>>,
    /// Whether the project access token is active or not.
    pub active: bool,
    /// When the project access token expires.
    pub expires_at: Option<NaiveDate>,
}
