// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! gitlab-runner-ctl
//!
//! A command line tool to inspect and manage a set of `gitlab-runner` instances registered to
//! GitLab.

use clap::builder::PossibleValuesParser;
use clap::{Arg, ArgAction};
use gitlab_runner_api::gitlab;
use gitlab_runner_api::gitlab::api::{self, AsyncQuery};
use log::LevelFilter;
use serde::Deserialize;
use thiserror::Error;

mod exit_code;
use exit_code::ExitCode;

mod actions;

#[derive(Debug, Error)]
#[non_exhaustive]
enum SetupError {
    #[error("unrecognized logger: `{}`", logger)]
    UnrecognizedLogger { logger: String },
    #[error("unknown command '{}'", command)]
    UnknownCommand { command: String },
    #[error("no GitLab API token provided")]
    MissingToken,
    #[error("could not connect to GitLab: {}", source)]
    NoGitlabConnection {
        #[source]
        source: gitlab::GitlabError,
    },
    #[error("GitLab API error: {}", source)]
    GitlabApi {
        #[source]
        source: gitlab::api::ApiError<gitlab::RestError>,
    },
    #[error("`delete` error: {}", source)]
    Delete {
        #[from]
        source: actions::DeleteError,
    },
    #[error("`list` error: {}", source)]
    List {
        #[from]
        source: actions::ListError,
    },
    #[error("`pause` error: {}", source)]
    Pause {
        #[from]
        source: actions::PauseError,
    },
    #[error("`unpause` error: {}", source)]
    Unpause {
        #[from]
        source: actions::UnpauseError,
    },
    #[error("`register` error: {}", source)]
    Register {
        #[from]
        source: actions::RegisterError,
    },
    #[error("`rename` error: {}", source)]
    Rename {
        #[from]
        source: actions::RenameError,
    },
    #[error("`jobs` error: {}", source)]
    Jobs {
        #[from]
        source: actions::JobsError,
    },
    #[error("`status` error: {}", source)]
    Status {
        #[from]
        source: actions::StatusError,
    },
    #[error("`tag` error: {}", source)]
    Tag {
        #[from]
        source: actions::TagError,
    },
    #[error("`token` error: {}", source)]
    Token {
        #[from]
        source: actions::TokenError,
    },
    #[error("`project-token` error: {}", source)]
    ProjectToken {
        #[from]
        source: actions::ProjectTokenError,
    },
}

impl SetupError {
    fn unrecognized_logger(logger: String) -> Self {
        SetupError::UnrecognizedLogger {
            logger,
        }
    }

    fn unknown_command(command: String) -> Self {
        SetupError::UnknownCommand {
            command,
        }
    }

    fn missing_token() -> Self {
        Self::MissingToken
    }

    fn no_gitlab_connection(source: gitlab::GitlabError) -> Self {
        Self::NoGitlabConnection {
            source,
        }
    }

    fn gitlab_api(source: gitlab::api::ApiError<gitlab::RestError>) -> Self {
        Self::GitlabApi {
            source,
        }
    }
}

enum Logger {
    Env,
}

#[derive(Debug, Deserialize)]
struct SelfUser {
    #[serde(default)]
    is_admin: Option<bool>,
}

async fn try_main() -> Result<ExitCode, SetupError> {
    let matches = clap::Command::new("grlctl")
        .version(clap::crate_version!())
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Manage GitLab Runners on a GitLab instance")
        .arg(
            Arg::new("DEBUG")
                .short('d')
                .long("debug")
                .help("Increase verbosity")
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("LOGGER")
                .short('l')
                .long("logger")
                .default_value("env")
                .value_parser(PossibleValuesParser::new(["env"]))
                .help("Logging backend")
                .value_name("LOGGER")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("GITLAB")
                .short('g')
                .long("gitlab")
                .default_value("gitlab.com")
                .env("GLRCTL_INSTANCE")
                .help("The GitLab instance to communicate with.")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("INSECURE")
                .long("insecure")
                .help("Communicate with GitLab without TLS.")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("TOKEN")
                .short('t')
                .long("token")
                .help("API token to use to communicate with GitLab")
                .action(ArgAction::Set),
        )
        .subcommand(actions::Delete::subcommand())
        .subcommand(actions::List::subcommand())
        .subcommand(actions::Pause::subcommand())
        .subcommand(actions::Unpause::subcommand())
        .subcommand(actions::Register::subcommand())
        .subcommand(actions::Rename::subcommand())
        .subcommand(actions::Jobs::subcommand())
        .subcommand(actions::Status::subcommand())
        .subcommand(actions::Tag::subcommand())
        .subcommand(actions::Token::subcommand())
        .subcommand(actions::ProjectToken::subcommand())
        .get_matches();

    let log_level = match matches.get_count("DEBUG") {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let _logger = match matches
        .get_one::<String>("LOGGER")
        .expect("logger should have a value")
        .as_str()
    {
        "env" => {
            env_logger::Builder::new().filter(None, log_level).init();
            Logger::Env
        },

        logger => {
            return Err(SetupError::unrecognized_logger(logger.into()));
        },
    };

    log::set_max_level(log_level);

    let client = {
        let host = matches
            .get_one::<String>("GITLAB")
            .expect("gitlab should have a value");
        let token = matches
            .get_one::<String>("TOKEN")
            .ok_or_else(SetupError::missing_token)?;
        let mut builder = gitlab::GitlabBuilder::new(host, token);

        if matches.get_flag("INSECURE") {
            builder.insecure();
        }

        builder
            .build_async()
            .await
            .map_err(SetupError::no_gitlab_connection)?
    };

    let is_admin = {
        let endpoint = api::users::CurrentUser::builder().build().unwrap();
        let self_user: SelfUser = endpoint
            .query_async(&client)
            .await
            .map_err(SetupError::gitlab_api)?;
        self_user.is_admin.unwrap_or(false)
    };

    let status = match matches.subcommand() {
        Some(("delete", m)) => actions::Delete::run(client, is_admin, m).await?,
        Some(("list", m)) => actions::List::run(client, is_admin, m).await?,
        Some(("pause", m)) => actions::Pause::run(client, is_admin, m).await?,
        Some(("unpause", m)) => actions::Unpause::run(client, is_admin, m).await?,
        Some(("register", m)) => actions::Register::run(client, m).await?,
        Some(("rename", m)) => actions::Rename::run(client, is_admin, m).await?,
        Some(("jobs", m)) => actions::Jobs::run(client, is_admin, m).await?,
        Some(("status", m)) => actions::Status::run(client, is_admin, m).await?,
        Some(("tag", m)) => actions::Tag::run(client, is_admin, m).await?,
        Some(("token", m)) => actions::Token::run(client, is_admin, m).await?,
        Some(("project-token", m)) => actions::ProjectToken::run(client, m).await?,
        Some((subcmd, _)) => return Err(SetupError::unknown_command(subcmd.into())),
        None => ExitCode::Success,
    };

    Ok(status)
}

#[tokio::main]
async fn main() {
    match try_main().await {
        Ok(code) => {
            code.exit();
            unreachable!()
        },
        Err(err) => panic!("{:?}", err),
    }
}
