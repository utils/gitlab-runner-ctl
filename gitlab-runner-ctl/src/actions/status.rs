// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::num::NonZeroUsize;

use clap::{Arg, ArgAction, ArgMatches, Command};
use futures_util::StreamExt;
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use itertools::Itertools;
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum StatusError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
    #[error("failed to fetch job: {}", source)]
    Job {
        #[from]
        source: ApiError<RestError>,
    },
}

impl StatusError {
    fn job(source: ApiError<RestError>) -> Self {
        Self::Job {
            source,
        }
    }
}

type StatusResult<T> = Result<T, StatusError>;

pub struct Status;

impl Status {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> StatusResult<ExitCode> {
        let runners = helpers::fetch_runners(&client, is_admin, matches, true).await?;

        // Get the job count to fetch
        let count = matches
            .get_one("JOBLIMIT")
            .and_then(|n| NonZeroUsize::new(*n));

        for (runner, details) in runners {
            // TODO: formatting
            println!("id: {}", details.id);
            if let Some(name) = details.name {
                println!("name: {}", name);
            }
            if let Some(description) = details.description {
                println!("description: {}", description);
            }
            println!("shared: {}", details.is_shared);
            println!("type: {:?}", details.runner_type);
            println!("tags: {}", details.tags.iter().join(", "));
            println!("paused: {}", details.paused);
            if let Some(online) = details.online {
                println!("online: {}", online);
            }
            if let Some(contacted_at) = details.contacted_at {
                println!("last contact: {}", contacted_at.to_rfc3339());
            }
            println!("access level: {:?}", details.access_level);
            if let Some(maximum_timeout) = details.maximum_timeout {
                println!("maximum timeout: {}", maximum_timeout);
            }
            if let Some(run_untagged) = details.run_untagged {
                println!("run untagged: {}", run_untagged);
            }
            println!();

            let jobs = runner.jobs(count, Vec::new()).collect::<Vec<_>>().await;
            if jobs.is_empty() {
                println!("no jobs");
            }
            for job in jobs {
                let job = job.map_err(StatusError::job)?;
                println!("job {}:", job.id);
                println!("    name: {}", job.name);
                println!("    stage: {}", job.stage);
                println!("    pipeline: {}", job.pipeline.id);
                println!("    ref: {}", job.ref_);
                println!("    tag: {}", job.tag);
                println!("    project: {}", job.project.path_with_namespace);
                println!("    commit: {}", job.commit.title);
                println!("    status: {:?}", job.status);
                if let Some(failure_reason) = job.failure_reason {
                    println!("    failure reason: {:?}", failure_reason);
                }
                println!("    created at: {}", job.created_at.to_rfc3339());
                if let Some(started_at) = job.started_at {
                    println!("    started at: {}", started_at.to_rfc3339());
                }
                if let Some(finished_at) = job.finished_at {
                    println!("    finished at: {}", finished_at.to_rfc3339());
                }
                if let Some(duration) = job.duration {
                    println!("    duration: {}", duration);
                }
                println!("    queued: {}", job.queued_duration);
                println!();
            }

            println!();
        }

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("status").about("report runner status").arg(
            Arg::new("JOBLIMIT")
                .short('N')
                .long("limit")
                .default_value("5")
                .value_parser(clap::value_parser!(usize))
                .help("Limit on the number of jobs to fetch (0 fetches all)")
                .value_name("JOBLIMIT")
                .action(ArgAction::Set),
        );
        filters::FilterOptions::add_options(cmd)
    }
}
