// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{ArgMatches, Command};
use gitlab_runner_api::gitlab::AsyncGitlab;
use thiserror::Error;

use crate::exit_code::ExitCode;

mod create;
mod list;
mod revoke;
mod rotate;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum TokenError {
    #[error("unknown command '{}'", command)]
    UnknownCommand { command: String },
    #[error("`create` error: {}", source)]
    Create {
        #[from]
        source: self::create::TokenCreateError,
    },
    #[error("`list` error: {}", source)]
    List {
        #[from]
        source: self::list::TokenListError,
    },
    #[error("`revoke` error: {}", source)]
    Revoke {
        #[from]
        source: self::revoke::TokenRevokeError,
    },
    #[error("`rotate` error: {}", source)]
    Rotate {
        #[from]
        source: self::rotate::TokenRotateError,
    },
}

impl TokenError {
    fn unknown_command(command: String) -> Self {
        Self::UnknownCommand {
            command,
        }
    }
}

type TokenResult<T> = Result<T, TokenError>;

pub struct Token;

impl Token {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> TokenResult<ExitCode> {
        let status = match matches.subcommand() {
            Some(("create", m)) => self::create::TokenCreate::run(client, is_admin, m).await?,
            Some(("list", m)) => self::list::TokenList::run(client, m).await?,
            Some(("revoke", m)) => self::revoke::TokenRevoke::run(client, m).await?,
            Some(("rotate", m)) => self::rotate::TokenRotate::run(client, m).await?,
            Some((subcmd, _)) => return Err(TokenError::unknown_command(subcmd.into())),
            None => ExitCode::Success,
        };

        Ok(status)
    }

    pub fn subcommand() -> Command {
        Command::new("token")
            .about("manage API tokens for runners")
            .subcommand(self::create::TokenCreate::subcommand())
            .subcommand(self::list::TokenList::subcommand())
            .subcommand(self::revoke::TokenRevoke::subcommand())
            .subcommand(self::rotate::TokenRotate::subcommand())
    }
}
