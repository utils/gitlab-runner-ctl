// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::actions;
use gitlab_runner_api::gitlab::api::runners::EditRunnerBuilder;
use gitlab_runner_api::gitlab::AsyncGitlab;
use gitlab_runner_api::RunnerDetails;
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum TagError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
}

#[derive(Debug, Clone)]
struct TagAction {
    tags_to_add: Vec<String>,
    tags_to_remove: Vec<String>,
    clear_tags: bool,
}

impl TagAction {
    fn new(mut tags_to_add: Vec<String>, tags_to_remove: Vec<String>, clear_tags: bool) -> Self {
        // Remove tags to add that are also requested to remove.
        tags_to_add.retain(|tag| !tags_to_remove.contains(tag));

        Self {
            tags_to_add,
            tags_to_remove,
            clear_tags,
        }
    }

    fn tags_for(&self, details: &RunnerDetails) -> Vec<String> {
        if self.clear_tags {
            self.tags_to_add.clone()
        } else {
            details
                .tags
                .iter()
                .chain(self.tags_to_add.iter())
                .filter(|tag| !self.tags_to_remove.contains(tag))
                .map(Into::into)
                .collect()
        }
    }
}

type TagResult<T> = Result<T, TagError>;

pub struct Tag;

impl Tag {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> TagResult<ExitCode> {
        let need_details = matches.contains_id("ADD_TAG") || matches.contains_id("REMOVE_TAG");
        let runners = helpers::fetch_runners(&client, is_admin, matches, need_details).await?;

        let tags_to_add: Vec<String> = matches
            .get_many("ADD_TAG")
            .map(|tags| tags.into_iter().cloned().collect())
            .unwrap_or_default();
        let tags_to_remove: Vec<String> = matches
            .get_many("REMOVE_TAG")
            .map(|tags| tags.into_iter().cloned().collect())
            .unwrap_or_default();
        let clear_tags = matches.get_flag("CLEAR_TAGS");
        let tag_action = TagAction::new(tags_to_add, tags_to_remove, clear_tags);
        let untagged_state = if matches.get_flag("UNTAGGED") {
            Some(true)
        } else if matches.get_flag("NO_UNTAGGED") {
            Some(false)
        } else {
            None
        };

        let mut code = ExitCode::Success;
        for (runner, details) in runners {
            let label = details
                .description
                .as_ref()
                .or(details.name.as_ref())
                .map(AsRef::as_ref)
                .unwrap_or("<unknown>");

            let tag_action = &tag_action;
            let details = &details;
            let action = move |builder: &mut EditRunnerBuilder| {
                let tags = actions::set_tags(tag_action.tags_for(details).into_iter());

                tags(builder);

                if let Some(untagged) = untagged_state {
                    let untagged = actions::untagged_jobs(untagged);
                    untagged(builder);
                }
            };

            match runner.update(action).await {
                Ok(_) => {
                    println!("Runner #{} ({}) tags have been updated", details.id, label);
                },
                Err(err) => {
                    log::error!(
                        "Failed to edit tags for runner #{} ({}): {:?}",
                        details.id,
                        label,
                        err,
                    );
                    code = ExitCode::Failure;
                },
            }
        }

        Ok(code)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("tag")
            .about("manage tag settings on runners")
            .arg(
                Arg::new("NO_UNTAGGED")
                    .short('t')
                    .long("no-untagged")
                    .action(ArgAction::SetTrue)
                    .overrides_with("UNTAGGED"),
            )
            .arg(
                Arg::new("UNTAGGED")
                    .short('u')
                    .long("untagged")
                    .action(ArgAction::SetTrue)
                    .overrides_with("NO_UNTAGGED"),
            )
            .arg(
                Arg::new("CLEAR_TAGS")
                    .short('C')
                    .long("clear-tags")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("ADD_TAG")
                    .short('a')
                    .long("add")
                    .action(ArgAction::Append),
            )
            .arg(
                Arg::new("REMOVE_TAG")
                    .short('r')
                    .long("remove")
                    .action(ArgAction::Append),
            );
        filters::FilterOptions::add_options(cmd)
    }
}
