// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::gitlab::api::runners::EditRunnerBuilder;
use gitlab_runner_api::gitlab::AsyncGitlab;
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum RenameError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
    #[error("too many runners returned: found {}", count)]
    TooMany { count: usize },
}

impl RenameError {
    fn too_many(count: usize) -> Self {
        Self::TooMany {
            count,
        }
    }
}

type RenameResult<T> = Result<T, RenameError>;

pub struct Rename;

impl Rename {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> RenameResult<ExitCode> {
        let runners = helpers::fetch_runners(&client, is_admin, matches, false).await?;
        let name = matches
            .get_one::<String>("NAME")
            .expect("`NAME` is required by the option parser");
        let rename_action = |builder: &mut EditRunnerBuilder| {
            builder.description(name.clone());
        };

        if runners.len() > 1 {
            return Err(RenameError::too_many(runners.len()));
        }

        let mut code = ExitCode::Success;
        for (runner, overview) in &runners {
            let label = overview
                .description
                .as_ref()
                .or(overview.name.as_ref())
                .map(AsRef::as_ref)
                .unwrap_or("<unknown>");

            if matches.get_flag("DRY_RUN") {
                println!(
                    "Runner #{} ({}) would be renamed to {}",
                    overview.id, label, name
                );
                continue;
            }

            match runner.update(&rename_action).await {
                Ok(_) => {
                    println!(
                        "Runner #{} ({}) has been renamed to {}",
                        overview.id, label, name
                    );
                },
                Err(err) => {
                    log::error!(
                        "Failed to rename runner #{} ({}): {:?}",
                        overview.id,
                        label,
                        err,
                    );
                    code = ExitCode::Failure;
                },
            }
        }

        Ok(code)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("rename")
            .about("rename a runner on an instance")
            .arg(Arg::new("NAME").short('N').long("name").required(true))
            .arg(
                Arg::new("DRY_RUN")
                    .short('n')
                    .long("dry-run")
                    .action(ArgAction::SetTrue),
            );
        filters::FilterOptions::add_options(cmd)
    }
}
