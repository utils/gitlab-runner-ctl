// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::fmt;

use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::{Cell, Row};
use gitlab_runner_api::{Job, RunnerJobStatus};
use thiserror::Error;

use crate::actions::list::columns::Column as RunnerColumn;

enum JobStatus {
    Running,
    Success,
    Failed,
    Canceled,
}

impl From<RunnerJobStatus> for JobStatus {
    fn from(status: RunnerJobStatus) -> JobStatus {
        match status {
            RunnerJobStatus::Running => Self::Running,
            RunnerJobStatus::Success => Self::Success,
            RunnerJobStatus::Failed => Self::Failed,
            RunnerJobStatus::Canceled => Self::Canceled,
        }
    }
}

impl fmt::Display for JobStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: Add styling
        let c = match self {
            Self::Running => '\u{23F1}',  // STOPWATCH
            Self::Success => '\u{2713}',  // CHECK MARK
            Self::Failed => '\u{2717}',   // BALLOT X
            Self::Canceled => '\u{2298}', // CIRCLE WITH SOLIDUS
        };
        write!(f, "{}", c)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum JobColumn {
    Id,
    Stage,
    Name,
    PipelineId,
    Ref,
    Tag,
    Project,
    Commit,
    //CommitId,
    //CommitTitle,
    //CommitMessage,
    Status,
    CreatedAt,
    StartedAt,
    FinishedAt,
    Duration,
    QueuedDuration,
    Runner(RunnerColumn),
}

impl JobColumn {
    fn cell_for(self, details: &Job) -> Cell {
        match self {
            Self::Id => Cell::new(details.id),
            Self::Stage => Cell::new(details.stage.clone()),
            Self::Name => Cell::new(details.name.clone()),
            Self::PipelineId => Cell::new(details.pipeline.id),
            Self::Ref => Cell::new(details.ref_.clone()),
            Self::Tag => Cell::new(details.tag),
            Self::Project => Cell::new(details.project.name.clone()),
            Self::Commit => Cell::new(details.commit.id.clone()),
            //Self::CommitId => Cell::new(details.commit.id),
            //Self::CommitTitle => Cell::new(details.commit.title),
            //Self::CommitMessage => Cell::new(details.commit.message),
            Self::Status => Cell::new(JobStatus::from(details.status)),
            Self::CreatedAt => Cell::new(details.created_at),
            Self::StartedAt => {
                if let Some(started_at) = details.started_at {
                    Cell::new(started_at)
                } else {
                    Cell::new("<queued>")
                }
            },
            Self::FinishedAt => {
                if let Some(finished_at) = details.finished_at {
                    Cell::new(finished_at)
                } else {
                    Cell::new("<in progress>")
                }
            },
            Self::Duration => {
                if let Some(duration) = details.duration {
                    Cell::new(duration)
                } else {
                    Cell::new("<incomplete>")
                }
            },
            Self::QueuedDuration => Cell::new(details.queued_duration),
            Self::Runner(column) => {
                if let Some(runner) = details.runner.as_ref() {
                    column.cell_for(runner)
                } else {
                    Cell::new("<no runner>")
                }
            },
        }
    }

    fn header(self) -> Cow<'static, str> {
        match self {
            Self::Id => "Id".into(),
            Self::Stage => "Stage".into(),
            Self::Name => "Name".into(),
            Self::PipelineId => "Pipeline Id".into(),
            Self::Ref => "Ref".into(),
            Self::Tag => "Tag".into(),
            Self::Project => "Project".into(),
            Self::Commit => "Commit".into(),
            // Self::CommitId => "Commit Id".into(),
            // Self::CommitTitle => "Commit Title".into(),
            // Self::CommitMessage => "Commit Description".into(),
            Self::Status => "Status".into(),
            Self::CreatedAt => "Created At".into(),
            Self::StartedAt => "Started At".into(),
            Self::FinishedAt => "Finished At".into(),
            Self::Duration => "Duration".into(),
            Self::QueuedDuration => "Queued Time".into(),
            Self::Runner(column) => format!("Runner {}", column.header()).into(),
        }
    }

    fn name(self) -> Cow<'static, str> {
        match self {
            Self::Id => "id".into(),
            Self::Stage => "stage".into(),
            Self::Name => "name".into(),
            Self::PipelineId => "pipline".into(),
            Self::Ref => "ref".into(),
            Self::Tag => "tag".into(),
            Self::Project => "project".into(),
            Self::Commit => "commit".into(),
            //Self::CommitId => "commit_id".into(),
            //Self::CommitTitle => "commit_title".into(),
            //Self::CommitMessage => "commit_description".into(),
            Self::Status => "status".into(),
            Self::CreatedAt => "created_at".into(),
            Self::StartedAt => "started_at".into(),
            Self::FinishedAt => "finished_at".into(),
            Self::Duration => "duration".into(),
            Self::QueuedDuration => "queued_time".into(),
            Self::Runner(column) => format!("runner/{}", column.name()).into(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct JobColumns {
    columns: Vec<JobColumn>,
}

impl JobColumns {
    pub fn headers(&self) -> Row {
        self.columns
            .iter()
            .map(|col| col.header())
            .collect::<Vec<_>>()
            .into()
    }

    pub fn row_for(&self, overview: &Job) -> Row {
        let mut row = Row::new();

        self.columns.iter().for_each(|col| {
            row.add_cell(col.cell_for(overview));
        });

        row
    }
}

impl Default for JobColumns {
    fn default() -> Self {
        Self {
            columns: vec![
                JobColumn::PipelineId,
                JobColumn::Id,
                JobColumn::Name,
                JobColumn::Status,
                JobColumn::CreatedAt,
                JobColumn::FinishedAt,
            ],
        }
    }
}

pub struct JobColumnOptions;

#[derive(Debug, Error)]
enum JobColumnParseError {
    #[error("unrecognized column: {}", column)]
    UnrecognizedColumn { column: String },
}

impl JobColumnParseError {
    fn unrecognized_column(column: &str) -> Self {
        Self::UnrecognizedColumn {
            column: column.into(),
        }
    }
}

macro_rules! column_pairs{
    [$($col:expr,)*] => {
        [$(($col, $col.name()),)*]
    };
}

impl JobColumnOptions {
    fn column_parser(arg: &str) -> Result<JobColumns, JobColumnParseError> {
        let all_columns = column_pairs![
            JobColumn::Id,
            JobColumn::Stage,
            JobColumn::Name,
            JobColumn::PipelineId,
            JobColumn::Ref,
            JobColumn::Tag,
            JobColumn::Project,
            JobColumn::Commit,
            JobColumn::Status,
            JobColumn::CreatedAt,
            JobColumn::StartedAt,
            JobColumn::FinishedAt,
            JobColumn::Duration,
            JobColumn::QueuedDuration,
            JobColumn::Runner(RunnerColumn::Id),
            JobColumn::Runner(RunnerColumn::Name),
            JobColumn::Runner(RunnerColumn::State),
            JobColumn::Runner(RunnerColumn::Type),
            JobColumn::Runner(RunnerColumn::Shared),
            JobColumn::Runner(RunnerColumn::Description),
            JobColumn::Runner(RunnerColumn::Label),
            JobColumn::Runner(RunnerColumn::MaintenanceNote),
            JobColumn::Runner(RunnerColumn::Tags),
            JobColumn::Runner(RunnerColumn::AccessLevel),
            JobColumn::Runner(RunnerColumn::MaximumTimeout),
            JobColumn::Runner(RunnerColumn::RunUntagged),
        ];

        let columns = arg
            .split(',')
            .map(|component| {
                let (column, _) = all_columns
                    .iter()
                    .find(|(_, name)| *name == component)
                    .ok_or_else(|| JobColumnParseError::unrecognized_column(component))?;

                Ok(*column)
            })
            .collect::<Result<Vec<_>, _>>()?;

        // TODO: find and reject duplicate columns

        Ok(JobColumns {
            columns,
        })
    }

    /// Add filter options to a command.
    pub fn add_options(cmd: Command) -> Command {
        cmd.arg(
            Arg::new("JOBCOLUMNS")
                .short('C')
                .long("columns")
                .value_parser(Self::column_parser)
                .help("Column expression")
                .value_name("JOBCOLUMNS")
                .action(ArgAction::Set),
        )
    }

    /// Build all provided filter options into a filter.
    pub fn build(matches: &ArgMatches) -> JobColumns {
        matches
            .get_one("JOBCOLUMNS")
            .cloned()
            .unwrap_or_else(JobColumns::default)
    }
}
