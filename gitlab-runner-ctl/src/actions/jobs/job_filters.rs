// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::{Job, RunnerJobStatus, RunnerJobsFilter};
use thiserror::Error;

#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum JobsFilter {
    /// Job with status
    Status(RunnerJobStatus),
    /// Job with status (filter after fetching)
    LateStatus(RunnerJobStatus),

    // JobsFilter modifiers.
    /// JobsFilter which must match all component filters.
    AllOf(Vec<JobsFilter>),
    /// JobsFilter which must match any of the component filters.
    AnyOf(Vec<JobsFilter>),
    /// Invert a filter.
    Not(Box<JobsFilter>),
}

impl JobsFilter {
    /// Invert a filter.
    pub fn not(filter: Self) -> Self {
        Self::Not(Box::new(filter))
    }

    pub fn query_filters(&self) -> Vec<Option<RunnerJobsFilter>> {
        match self {
            Self::Status(status) => vec![Some(RunnerJobsFilter::Status((*status).into()))],
            Self::AllOf(filters) => {
                filters
                    .iter()
                    .flat_map(|filter| filter.query_filters())
                    .collect()
            },
            _ => Vec::new(),
        }
    }

    /// Check whether a filter matches a given runner's details.
    pub fn matches(&self, details: &Job) -> bool {
        match self {
            Self::Status(status) | Self::LateStatus(status) => details.status == *status,
            Self::AllOf(filters) => filters.iter().all(|f| f.matches(details)),
            Self::AnyOf(filters) => filters.iter().any(|f| f.matches(details)),
            Self::Not(filter) => !filter.matches(details),
        }
    }
}

pub struct JobsFilterOptions;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum JobsFilterParseError {
    #[error("unrecognized job filter: {}", filter)]
    UnrecognizedFilter { filter: String },
    #[error("unrecognized job status: {}", status)]
    UnrecognizedStatus { status: String },
}

impl JobsFilterParseError {
    fn unrecognized_filter(filter: &str) -> Self {
        Self::UnrecognizedFilter {
            filter: filter.into(),
        }
    }

    fn unrecognized_status(status: &str) -> Self {
        Self::UnrecognizedStatus {
            status: status.into(),
        }
    }
}

impl JobsFilterOptions {
    fn filter_parser(arg: &str) -> Result<JobsFilter, JobsFilterParseError> {
        let filters = arg
            .split(',')
            .map(|component| {
                let (invert, component) = if let Some(rest) = component.strip_prefix('!') {
                    (true, rest)
                } else {
                    (false, component)
                };

                let filter = if let Some(status) = component.strip_prefix("status=") {
                    JobsFilter::Status(match status {
                        "running" => RunnerJobStatus::Running,
                        "success" => RunnerJobStatus::Success,
                        "failed" => RunnerJobStatus::Failed,
                        "canceled" => RunnerJobStatus::Canceled,
                        _ => return Err(JobsFilterParseError::unrecognized_status(status)),
                    })
                } else if let Some(status) = component.strip_prefix("status*=") {
                    JobsFilter::LateStatus(match status {
                        "running" => RunnerJobStatus::Running,
                        "success" => RunnerJobStatus::Success,
                        "failed" => RunnerJobStatus::Failed,
                        "canceled" => RunnerJobStatus::Canceled,
                        _ => return Err(JobsFilterParseError::unrecognized_status(status)),
                    })
                } else {
                    return Err(JobsFilterParseError::unrecognized_filter(component));
                };

                Ok(if invert {
                    JobsFilter::not(filter)
                } else {
                    filter
                })
            })
            .collect::<Result<Vec<_>, _>>()?;

        Ok(JobsFilter::AnyOf(filters))
    }

    /// Add filter options to a command.
    pub fn add_options(cmd: Command) -> Command {
        cmd.arg(
            Arg::new("JOBFILTER")
                .short('J')
                .long("job-filter")
                .value_parser(Self::filter_parser)
                .help("Jobs Filter expression")
                .value_name("JOBFILTER")
                .action(ArgAction::Append),
        )
    }

    /// Build all provided filter options into a filter.
    pub fn build(matches: &ArgMatches) -> JobsFilter {
        let filters = if let Some(matches) = matches.get_many("JOBFILTER") {
            matches.into_iter().cloned().collect()
        } else {
            Vec::new()
        };

        JobsFilter::AllOf(filters)
    }
}
