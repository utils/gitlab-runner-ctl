// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{ArgMatches, Command};
use comfy_table::Table;
use gitlab_runner_api::gitlab::AsyncGitlab;
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

pub mod columns;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum ListError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
}

type ListResult<T> = Result<T, ListError>;

pub struct List;

impl List {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> ListResult<ExitCode> {
        let columns = columns::ColumnOptions::build(matches);
        let runners =
            helpers::fetch_runners(&client, is_admin, matches, columns.need_details()).await?;

        // TODO: sorting

        let mut table = Table::new();
        // TODO: apply formatting

        table.set_header(columns.headers());
        for (_, details) in runners {
            table.add_row(columns.row_for(&details));
        }
        println!("{}", table);

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("list").about("list runners on an instance");
        let cmd = filters::FilterOptions::add_options(cmd);
        columns::ColumnOptions::add_options(cmd)
    }
}
