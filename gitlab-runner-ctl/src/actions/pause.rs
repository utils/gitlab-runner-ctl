// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::VecDeque;
use std::num::NonZeroUsize;
use std::time::Duration;

use clap::{Arg, ArgAction, ArgMatches, Command};
use either::Either;
use futures_util::StreamExt;
use gitlab_runner_api::actions;
use gitlab_runner_api::gitlab::api::runners::EditRunnerBuilder;
use gitlab_runner_api::gitlab::AsyncGitlab;
use gitlab_runner_api::{RunnerJobStatus, RunnerJobsFilter};
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum PauseError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
}

type PauseResult<T> = Result<T, PauseError>;

pub struct Pause;

// Wait no more than 15 seconds between polling runners.
const MAX_DELAY: Duration = Duration::new(15, 0);

impl Pause {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> PauseResult<ExitCode> {
        let runners = helpers::fetch_runners(&client, is_admin, matches, false).await?;
        let maint_note = matches.get_one::<String>("MAINTENANCE_NOTE").cloned();
        let pause_action = |builder: &mut EditRunnerBuilder| {
            actions::pause(builder);
            if let Some(maint_note) = maint_note.as_ref() {
                builder.maintenance_note(maint_note.clone());
            }
        };

        let mut code = ExitCode::Success;
        let mut wait_for_runners = VecDeque::new();
        for (runner, overview) in &runners {
            let label = overview
                .description
                .as_ref()
                .or(overview.name.as_ref())
                .map(AsRef::as_ref)
                .unwrap_or("<unknown>");

            if matches.get_flag("DRY_RUN") {
                if overview.paused {
                    println!("Runner #{} ({}) is already paused", overview.id, label);
                } else {
                    println!("Runner #{} ({}) would be paused", overview.id, label);
                }
                continue;
            }

            match runner.update(&pause_action).await {
                Ok(_) => {
                    println!("Runner #{} ({}) has been paused", overview.id, label);
                    wait_for_runners.push_back(Either::Left((runner, overview)));
                },
                Err(err) => {
                    log::error!(
                        "Failed to pause runner #{} ({}): {:?}",
                        overview.id,
                        label,
                        err,
                    );
                    code = ExitCode::Failure;
                },
            }
        }

        if matches.get_flag("WAIT") {
            wait_for_runners.push_back(Either::Right(Duration::ZERO));
            let filters = vec![Some(RunnerJobsFilter::Status(
                RunnerJobStatus::Running.into(),
            ))];
            let count = NonZeroUsize::new(1);
            while let Some(runner_or_wait) = wait_for_runners.pop_front() {
                match runner_or_wait {
                    Either::Left((runner, details)) => {
                        let mut running_jobs = runner
                            .jobs(count, filters.clone())
                            .collect::<Vec<_>>()
                            .await;
                        if let Some(job) = running_jobs.pop() {
                            let label = details
                                .description
                                .as_ref()
                                .or(details.name.as_ref())
                                .map(AsRef::as_ref)
                                .unwrap_or("<unknown>");

                            match job {
                                Ok(job) => {
                                    println!(
                                        "Runner #{} ({}) is running job #{} ({}) for {}: duration {}",
                                        details.id,
                                        label,
                                        job.id,
                                        job.name,
                                        job.project.path_with_namespace,
                                        job.duration.unwrap_or(-1.),
                                    );
                                },
                                Err(err) => {
                                    println!(
                                        "Runner #{} ({}) is running a job (failed to query: {})",
                                        details.id, label, err,
                                    );
                                },
                            }
                            wait_for_runners.push_back(Either::Left((runner, details)));
                        }
                    },
                    Either::Right(delay) => {
                        // Only perform a delay if there is more work to do.
                        if !wait_for_runners.is_empty() {
                            // Delay further checks to avoid hammering the API.
                            std::thread::sleep(delay);

                            let new_delay = if delay.is_zero() {
                                Duration::new(1, 0)
                            } else if delay < MAX_DELAY {
                                delay * 2
                            } else {
                                MAX_DELAY
                            };

                            wait_for_runners.push_back(Either::Right(new_delay));
                        }
                    },
                }
            }
        }

        Ok(code)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("pause")
            .about("pause runners on an instance")
            .arg(
                Arg::new("MAINTENANCE_NOTE")
                    .short('M')
                    .long("maintenance-note")
                    .value_name("MAINTENANCE_NOTE")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("DRY_RUN")
                    .short('n')
                    .long("dry-run")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("WAIT")
                    .short('w')
                    .long("wait")
                    .action(ArgAction::SetTrue),
            );
        filters::FilterOptions::add_options(cmd)
    }
}
