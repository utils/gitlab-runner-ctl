// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::num::NonZeroUsize;

use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::Table;
use futures_util::TryStreamExt;
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

mod columns;
mod job_filters;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum JobsError {
    #[error("failed to fetch runner jobs: {}", source)]
    Runners {
        #[from]
        source: helpers::FetchError,
    },
    #[error("failed to fetch jobs: {}", source)]
    Fetch {
        #[from]
        source: ApiError<RestError>,
    },
}

type JobsResult<T> = Result<T, JobsError>;

impl JobsError {
    fn runners(source: helpers::FetchError) -> Self {
        Self::Runners {
            source,
        }
    }

    fn fetch(source: ApiError<RestError>) -> Self {
        Self::Fetch {
            source,
        }
    }
}

pub struct Jobs;

impl Jobs {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> JobsResult<ExitCode> {
        let runners = helpers::fetch_runners(&client, is_admin, matches, false)
            .await
            .map_err(JobsError::runners)?;

        let columns = columns::JobColumnOptions::build(matches);
        let job_filters = job_filters::JobsFilterOptions::build(matches);

        if matches.get_flag("BATCH") {
            let mut table = Table::new();
            table.set_header(columns.headers());

            for (runner, details) in runners {
                // Get the job count to fetch
                let count = matches
                    .get_one("JOBLIMIT")
                    .and_then(|n| NonZeroUsize::new(*n));

                let mut jobs = runner
                    .jobs(count, job_filters.query_filters())
                    .try_collect::<Vec<_>>()
                    .await
                    .map_err(JobsError::fetch)?;
                jobs.retain(|j| job_filters.matches(j));
                for mut job in jobs {
                    // We found the job from this runner; inject its details.
                    job.runner = Some(details.clone());
                    table.add_row(columns.row_for(&job));
                }
            }

            println!("{}", table);
        } else {
            for (runner, details) in runners {
                let runner_desc: Cow<str> = match details.description.as_ref() {
                    Some(desc) => desc.into(),
                    None => {
                        match details.name.as_ref() {
                            Some(name) => name.into(),
                            None => details.id.to_string().into(),
                        }
                    },
                };
                println!("Jobs for Runner: {}", runner_desc);
                let mut table = Table::new();
                table.set_header(columns.headers());

                // Get the job count to fetch
                let count = matches
                    .get_one("JOBLIMIT")
                    .and_then(|n| NonZeroUsize::new(*n));

                let mut jobs = runner
                    .jobs(count, job_filters.query_filters())
                    .try_collect::<Vec<_>>()
                    .await
                    .map_err(JobsError::fetch)?;
                jobs.retain(|j| job_filters.matches(j));
                if !jobs.is_empty() {
                    for mut job in jobs {
                        job.runner = Some(details.clone());
                        table.add_row(columns.row_for(&job));
                    }
                    println!("{}", table);
                } else {
                    println!("no jobs");
                }
            }
        }

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("jobs")
            .about("list jobs run on collection of runners")
            .arg(
                Arg::new("BATCH")
                    .short('B')
                    .long("batch")
                    .help("Batch all job results into a single table")
                    .value_name("BATCH")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("JOBLIMIT")
                    .short('N')
                    .long("limit")
                    .value_parser(clap::value_parser!(usize))
                    .help("Limit on the number of jobs to fetch (0 fetches all)")
                    .value_name("JOBLIMIT")
                    .action(ArgAction::Set),
            );
        let cmd = filters::FilterOptions::add_options(cmd);
        let cmd = job_filters::JobsFilterOptions::add_options(cmd);
        columns::JobColumnOptions::add_options(cmd)
    }
}
