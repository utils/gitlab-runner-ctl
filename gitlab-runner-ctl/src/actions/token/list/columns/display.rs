// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::fmt;

pub struct BoolMarkup {
    value: bool,
}

impl BoolMarkup {
    pub fn new(value: bool) -> Self {
        Self {
            value,
        }
    }
}

impl fmt::Display for BoolMarkup {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: Add styling
        write!(f, "{}", self.value)
    }
}
