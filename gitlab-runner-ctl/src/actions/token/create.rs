// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::NaiveDate;
use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::gitlab::api::{self, ApiError, AsyncQuery};
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::{
    PersonalAccessToken, PersonalAccessTokenParameters, PersonalAccessTokenScope,
};
use itertools::Itertools;
use serde::Deserialize;
use thiserror::Error;

use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum TokenCreateError {
    #[error("token creation as a user requires an administrator token")]
    AsUserRequiresAdmin {},
    #[error("failed to search for users: {}", source)]
    UserSearch {
        #[source]
        source: ApiError<RestError>,
    },
    #[error("failed to find user '{}'", user)]
    NoSuchUser { user: String },
    #[error("failed to create token: {}", source)]
    CreateToken {
        #[source]
        source: ApiError<RestError>,
    },
}

impl TokenCreateError {
    fn as_user_requires_admin() -> Self {
        Self::AsUserRequiresAdmin {}
    }

    fn user_search(source: ApiError<RestError>) -> Self {
        Self::UserSearch {
            source,
        }
    }

    fn no_such_user(user: String) -> Self {
        Self::NoSuchUser {
            user,
        }
    }

    fn create_token(source: ApiError<RestError>) -> Self {
        Self::CreateToken {
            source,
        }
    }
}

type TokenCreateResult<T> = Result<T, TokenCreateError>;

pub struct TokenCreate;

#[derive(Debug, Deserialize)]
struct User {
    id: u64,
    username: String,
}

#[derive(Debug, Error)]
enum ScopeParserError {
    #[error("invalid scope: {}", scope)]
    InvalidScope { scope: String },
}

impl ScopeParserError {
    fn invalid_scope(scope: &str) -> Self {
        Self::InvalidScope {
            scope: scope.into(),
        }
    }
}

fn parse_scope(arg: &str) -> Result<PersonalAccessTokenScope, ScopeParserError> {
    match arg {
        "api" => Ok(PersonalAccessTokenScope::Api),
        "read_user" => Ok(PersonalAccessTokenScope::ReadUser),
        "read_api" => Ok(PersonalAccessTokenScope::ReadApi),
        "read_repository" => Ok(PersonalAccessTokenScope::ReadRepository),
        "write_repository" => Ok(PersonalAccessTokenScope::WriteRepository),
        "read_registry" => Ok(PersonalAccessTokenScope::ReadRegistry),
        "write_registry" => Ok(PersonalAccessTokenScope::WriteRegistry),
        "sudo" => Ok(PersonalAccessTokenScope::Sudo),
        "admin_mode" => Ok(PersonalAccessTokenScope::AdminMode),
        "create_runner" => Ok(PersonalAccessTokenScope::CreateRunner),
        "ai_features" => Ok(PersonalAccessTokenScope::AiFeatures),
        "k8s_proxy" => Ok(PersonalAccessTokenScope::K8sProxy),
        "read_service_ping" => Ok(PersonalAccessTokenScope::ReadServicePing),
        _ => Err(ScopeParserError::invalid_scope(arg)),
    }
}

fn scope_api(
    scope: &PersonalAccessTokenScope,
) -> Option<api::users::personal_access_tokens::PersonalAccessTokenScope> {
    match scope {
        PersonalAccessTokenScope::Api => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::Api)
        },
        PersonalAccessTokenScope::ReadUser => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::ReadUser)
        },
        PersonalAccessTokenScope::ReadApi => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::ReadApi)
        },
        PersonalAccessTokenScope::ReadRepository => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::ReadRepository)
        },
        PersonalAccessTokenScope::WriteRepository => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::WriteRepository)
        },
        PersonalAccessTokenScope::ReadRegistry => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::ReadRegistry)
        },
        PersonalAccessTokenScope::WriteRegistry => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::WriteRegistry)
        },
        PersonalAccessTokenScope::Sudo => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::Sudo)
        },
        PersonalAccessTokenScope::AdminMode => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::AdminMode)
        },
        PersonalAccessTokenScope::CreateRunner => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::CreateRunner)
        },
        PersonalAccessTokenScope::AiFeatures => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::AiFeatures)
        },
        PersonalAccessTokenScope::K8sProxy => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::K8sProxy)
        },
        PersonalAccessTokenScope::ReadServicePing => {
            Some(api::users::personal_access_tokens::PersonalAccessTokenScope::ReadServicePing)
        },
        scope => {
            log::error!("Scope conversion failure: {:?}", scope);
            None
        },
    }
}

impl TokenCreate {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> TokenCreateResult<ExitCode> {
        let as_user = matches.get_one::<String>("AS").cloned();
        if !is_admin && as_user.is_some() {
            return Err(TokenCreateError::as_user_requires_admin());
        }
        let user_id = if let Some(as_user) = as_user.as_ref() {
            let endpoint = api::users::Users::builder()
                .username(as_user)
                .build()
                .unwrap();
            let users: Vec<User> = endpoint
                .query_async(&client)
                .await
                .map_err(TokenCreateError::user_search)?;

            Some(
                users
                    .into_iter()
                    .find(|user| user.username == *as_user)
                    .map(|user| user.id)
                    .ok_or_else(|| TokenCreateError::no_such_user(as_user.into()))?,
            )
        } else {
            None
        };

        let mut builder = PersonalAccessTokenParameters::builder();
        builder
            .name(
                matches
                    .get_one::<String>("NAME")
                    .expect("NAME is a required argument"),
            )
            .scopes(
                matches
                    .get_many::<PersonalAccessTokenScope>("SCOPE")
                    .expect("SCOPE is a required argument")
                    .filter_map(scope_api),
            );
        if let Some(expires_at) = matches.get_one::<NaiveDate>("EXPIRATION") {
            builder.expires_at(*expires_at);
        }
        let params = builder.build().unwrap();

        let (_, details) = if let Some(user_id) = user_id {
            PersonalAccessToken::create_for(client, user_id, params)
                .await
                .map_err(TokenCreateError::create_token)?
        } else {
            PersonalAccessToken::create(client, params)
                .await
                .map_err(TokenCreateError::create_token)?
        };

        println!("ID: {}", details.details.id);
        println!("Name: {}", details.details.name);
        println!(
            "Expires: {}",
            details
                .details
                .expires_at
                .map(|e| e.to_string())
                .unwrap_or_else(|| "<never>".into()),
        );
        println!(
            "Scopes: {}",
            details.details.scopes.iter().map(|s| s.as_str()).join(", "),
        );
        println!("Token: {}", details.token);

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        Command::new("create")
            .about("create an API token for a user")
            .arg(
                Arg::new("AS")
                    .short('u')
                    .long("as-user")
                    .help("Create a token as a user (requires an admin token)")
                    .value_name("AS_USER")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("NAME")
                    .short('n')
                    .long("name")
                    .help("Name of the token")
                    .value_name("NAME")
                    .required(true)
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("EXPIRATION")
                    .short('e')
                    .long("expiration")
                    .help("Expiration date")
                    .value_parser(clap::value_parser!(NaiveDate))
                    .value_name("EXPIRATION")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("SCOPE")
                    .short('s')
                    .long("scope")
                    .help("Add a scope to the token")
                    .value_parser(parse_scope)
                    .value_name("SCOPE")
                    .required(true)
                    .action(ArgAction::Append),
            )
    }
}
