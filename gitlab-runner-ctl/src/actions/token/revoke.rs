// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::{Row, Table};
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::PersonalAccessToken;
use thiserror::Error;

use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum TokenRevokeError {
    #[error("failed to get details about token '{}': {}", id, source)]
    TokenDetails {
        id: u64,
        #[source]
        source: ApiError<RestError>,
    },
    #[error("failed to revoke token: {}", source)]
    RevokeToken {
        #[source]
        source: ApiError<RestError>,
    },
    #[error("failed to revoke client token: {}", source)]
    RevokeSelf {
        #[source]
        source: ApiError<RestError>,
    },
}

impl TokenRevokeError {
    fn token_details(id: u64, source: ApiError<RestError>) -> Self {
        Self::TokenDetails {
            id,
            source,
        }
    }

    fn revoke_token(source: ApiError<RestError>) -> Self {
        Self::RevokeToken {
            source,
        }
    }

    fn revoke_self(source: ApiError<RestError>) -> Self {
        Self::RevokeSelf {
            source,
        }
    }
}

type TokenRevokeResult<T> = Result<T, TokenRevokeError>;

pub struct TokenRevoke;

impl TokenRevoke {
    pub async fn run(client: AsyncGitlab, matches: &ArgMatches) -> TokenRevokeResult<ExitCode> {
        let mut revoked = Vec::new();
        let mut errors = Vec::new();

        if let Some(token_ids) = matches.get_many::<u64>("TOKEN_ID") {
            for id in token_ids {
                match PersonalAccessToken::by_id(client.clone(), *id).await {
                    Ok((token, details)) => {
                        match token.revoke().await {
                            Ok(()) => revoked.push(details),
                            Err(err) => {
                                errors.push((Some(details), TokenRevokeError::revoke_token(err)))
                            },
                        }
                    },
                    Err(err) => errors.push((None, TokenRevokeError::token_details(*id, err))),
                }
            }
        }

        if matches.get_flag("SELF") {
            if let Err(err) = PersonalAccessToken::revoke_client(client).await {
                errors.push((None, TokenRevokeError::revoke_self(err)));
            }
        }

        let revoked_table = {
            let mut table = Table::new();

            let mut header = Row::new();
            header.add_cell("ID".into());
            header.add_cell("Name".into());
            header.add_cell("User".into());
            header.add_cell("Last Used".into());

            table.set_header(header);
            for details in revoked {
                let mut row = Row::new();
                row.add_cell(details.id.into());
                row.add_cell(details.name.into());
                row.add_cell(details.user_id.into()); // TODO: get a username
                row.add_cell(if let Some(last_used_at) = details.last_used_at {
                    last_used_at.into()
                } else {
                    "<never>".into()
                });

                table.add_row(row);
            }

            table
        };

        let error_table = {
            let mut table = Table::new();

            let mut header = Row::new();
            header.add_cell("ID".into());
            header.add_cell("Name".into());
            header.add_cell("Error".into());

            table.set_header(header);
            for (details, err) in errors {
                let id = details
                    .as_ref()
                    .map(|d| d.id.into())
                    .unwrap_or_else(|| "<unknown>".into());
                let name = details
                    .as_ref()
                    .map(|d| d.name.as_str().into())
                    .unwrap_or_else(|| "<unknown>".into());

                let mut row = Row::new();
                row.add_cell(id);
                row.add_cell(name);
                row.add_cell(err.into());

                table.add_row(row);
            }

            table
        };

        if !revoked_table.is_empty() {
            println!("Revoked tokens\n{}", revoked_table);
        }

        let status = if !error_table.is_empty() {
            println!("Error tokens\n{}", error_table);
            ExitCode::Failure
        } else {
            ExitCode::Success
        };

        Ok(status)
    }

    pub fn subcommand() -> Command {
        Command::new("revoke")
            .about("revoke an API token")
            .arg(
                Arg::new("SELF")
                    .short('s')
                    .long("self")
                    .help("Revoke the token used to access GitLab")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("TOKEN_ID")
                    .help("Token ID")
                    .value_name("TOKEN_ID")
                    .value_parser(clap::value_parser!(u64))
                    .action(ArgAction::Append),
            )
    }
}
