// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::NaiveDate;
use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::{Row, Table};
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::PersonalAccessToken;
use thiserror::Error;

use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum TokenRotateError {
    #[error("failed to get details about token: {}", source)]
    TokenDetails {
        #[source]
        source: ApiError<RestError>,
    },
    #[error("failed to rotate token: {}", source)]
    RotateToken {
        #[source]
        source: ApiError<RestError>,
    },
    #[error("failed to rotate client token: {}", source)]
    RotateSelf {
        #[source]
        source: ApiError<RestError>,
    },
}

impl TokenRotateError {
    fn token_details(source: ApiError<RestError>) -> Self {
        Self::TokenDetails {
            source,
        }
    }

    fn rotate_token(source: ApiError<RestError>) -> Self {
        Self::RotateToken {
            source,
        }
    }

    fn rotate_self(source: ApiError<RestError>) -> Self {
        Self::RotateSelf {
            source,
        }
    }
}

type TokenRotateResult<T> = Result<T, TokenRotateError>;

pub struct TokenRotate;

impl TokenRotate {
    pub async fn run(client: AsyncGitlab, matches: &ArgMatches) -> TokenRotateResult<ExitCode> {
        let mut rotated = Vec::new();
        let mut errors = Vec::new();

        let expiration = matches.get_one::<NaiveDate>("EXPIRATION").copied();

        if let Some(token_ids) = matches.get_many::<u64>("TOKEN_ID") {
            for id in token_ids {
                match PersonalAccessToken::by_id(client.clone(), *id).await {
                    Ok((token, details)) => {
                        match token.rotate(expiration).await {
                            Ok((_, details)) => rotated.push(details),
                            Err(err) => {
                                errors.push((Some(details), TokenRotateError::rotate_token(err)))
                            },
                        }
                    },
                    Err(err) => errors.push((None, TokenRotateError::token_details(err))),
                }
            }
        }

        if matches.get_flag("SELF") {
            if let Err(err) = PersonalAccessToken::rotate_client(client, expiration).await {
                errors.push((None, TokenRotateError::rotate_self(err)));
            }
        }

        let rotated_table = {
            let mut table = Table::new();

            let mut header = Row::new();
            header.add_cell("ID".into());
            header.add_cell("Name".into());
            header.add_cell("User".into());
            header.add_cell("Token".into());

            table.set_header(header);
            for new_token in rotated {
                let details = new_token.details;

                let mut row = Row::new();
                row.add_cell(details.id.into());
                row.add_cell(details.name.into());
                row.add_cell(details.user_id.into()); // TODO: get a username
                row.add_cell(new_token.token.into());

                table.add_row(row);
            }

            table
        };

        let error_table = {
            let mut table = Table::new();

            let mut header = Row::new();
            header.add_cell("ID".into());
            header.add_cell("Name".into());
            header.add_cell("Error".into());

            table.set_header(header);
            for (details, err) in errors {
                let id = details
                    .as_ref()
                    .map(|d| d.id.into())
                    .unwrap_or_else(|| "<unknown>".into());
                let name = details
                    .as_ref()
                    .map(|d| d.name.as_str().into())
                    .unwrap_or_else(|| "<unknown>".into());

                let mut row = Row::new();
                row.add_cell(id);
                row.add_cell(name);
                row.add_cell(err.into());

                table.add_row(row);
            }

            table
        };

        if !rotated_table.is_empty() {
            println!("Rotated tokens\n{}", rotated_table);
        }

        let status = if !error_table.is_empty() {
            println!("Error tokens\n{}", error_table);
            ExitCode::Failure
        } else {
            ExitCode::Success
        };

        Ok(status)
    }

    pub fn subcommand() -> Command {
        Command::new("rotate")
            .about("rotate an API token")
            .arg(
                Arg::new("SELF")
                    .short('s')
                    .long("self")
                    .help("Rotate the token used to access GitLab")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("EXPIRATION")
                    .short('e')
                    .long("expiration")
                    .help("Expiration date")
                    .value_name("EXPIRATION")
                    .value_parser(clap::value_parser!(NaiveDate))
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("TOKEN_ID")
                    .help("Token ID")
                    .value_name("TOKEN_ID")
                    .value_parser(clap::value_parser!(u64))
                    .action(ArgAction::Append),
            )
    }
}
