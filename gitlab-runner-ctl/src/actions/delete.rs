// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::gitlab::AsyncGitlab;
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum DeleteError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
}

type DeleteResult<T> = Result<T, DeleteError>;

pub struct Delete;

impl Delete {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> DeleteResult<ExitCode> {
        let runners = helpers::fetch_runners(&client, is_admin, matches, false).await?;

        for (runner, details) in runners {
            let label = details
                .description
                .as_ref()
                .or(details.name.as_ref())
                .map(AsRef::as_ref)
                .unwrap_or("<unknown>");
            if matches.get_flag("DRY_RUN") {
                println!("Would delete {}", label);
            } else {
                println!("Deleting {}...", label);
                if let Err(err) = runner.delete().await {
                    println!("Failed to delete {}: {:?}", label, err);
                } else {
                    println!("Deleted {}", label);
                }
            }
        }

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("delete")
            .about("delete runners on an instance")
            .arg(
                Arg::new("DRY_RUN")
                    .short('n')
                    .long("dry-run")
                    .action(ArgAction::SetTrue),
            );
        filters::FilterOptions::add_options(cmd)
    }
}
