// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::fmt;

use gitlab_runner_api::{RunnerAccessLevel, RunnerDetails, RunnerType};

pub enum RunnerState {
    Online,
    Offline,
    PausedOnline,
    PausedOffline,
}

impl RunnerState {
    pub fn new(details: &RunnerDetails) -> Self {
        match (details.paused, details.online.unwrap_or(false)) {
            (true, true) => Self::PausedOnline,
            (true, false) => Self::PausedOffline,
            (false, true) => Self::Online,
            (false, false) => Self::Offline,
        }
    }
}

impl fmt::Display for RunnerState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: Add styling
        let c = match self {
            Self::Online => '\u{2713}',        // CHECK MARK
            Self::Offline => '\u{2717}',       // BALLOT X
            Self::PausedOnline => '\u{2611}',  // BALLOT BOX WITH CHECK
            Self::PausedOffline => '\u{2610}', // BALLOT BOX WITH X
        };
        write!(f, "{}", c)
    }
}

pub fn runner_type(rtype: RunnerType) -> &'static str {
    match rtype {
        RunnerType::Instance => "I",
        RunnerType::Group => "G",
        RunnerType::Project => "P",
    }
}

pub fn runner_access_level(access: RunnerAccessLevel) -> &'static str {
    match access {
        RunnerAccessLevel::NotProtected => "open",
        RunnerAccessLevel::RefProtected => "prot",
    }
}

pub fn runner_maximum_timeout(timeout: Option<u64>) -> Cow<'static, str> {
    if let Some(timeout) = timeout {
        format!("{}", timeout).into()
    } else {
        "<none>".into()
    }
}

pub fn runner_run_untagged(run_untagged: Option<bool>) -> Cow<'static, str> {
    if let Some(run_untagged) = run_untagged {
        format!("{}", BoolMarkup::new(run_untagged)).into()
    } else {
        "<unknown>".into()
    }
}

pub struct BoolMarkup {
    value: bool,
}

impl BoolMarkup {
    pub fn new(value: bool) -> Self {
        Self {
            value,
        }
    }
}

impl fmt::Display for BoolMarkup {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: Add styling
        write!(f, "{}", self.value)
    }
}
