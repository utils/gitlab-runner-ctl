// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::gitlab::api::runners::{RunnerAccessLevel, RunnerType};
use gitlab_runner_api::gitlab::api::users::CreateRunnerBuilder;
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::Runner;
use itertools::Itertools;
use thiserror::Error;

use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum RegisterError {
    #[error("failed to register runner: {}", source)]
    Register {
        #[source]
        source: ApiError<RestError>,
    },
    #[error("group and project runners require an ID")]
    MissingId {},
}

impl RegisterError {
    fn register(source: ApiError<RestError>) -> Self {
        Self::Register {
            source,
        }
    }

    fn missing_id() -> Self {
        Self::MissingId {}
    }
}

type RegisterResult<T> = Result<T, RegisterError>;

pub struct Register;

impl Register {
    pub async fn run(client: AsyncGitlab, matches: &ArgMatches) -> RegisterResult<ExitCode> {
        let id = || {
            matches
                .get_one::<u64>("ID")
                .ok_or_else(RegisterError::missing_id)
        };
        let (runner_type, id) = match matches.get_one::<String>("RUNNER_TYPE").map(String::as_str) {
            Some("instance") => (RunnerType::Instance, None),
            Some("group") => (RunnerType::Group, Some(id()?)),
            Some("project") => (RunnerType::Project, Some(id()?)),
            Some(runner_type) => panic!("`RUNNER_TYPE` did not reject '{}' value", runner_type),
            None => panic!("`RUNNER_TYPE` is required by the option parser"),
        };
        let description = matches
            .get_one::<String>("DESCRIPTION")
            .expect("`DESCRIPTION` is required by the option parser")
            .clone();
        let paused = matches.get_flag("PAUSED");
        let locked = matches.get_flag("LOCKED");
        let tags = matches
            .get_many::<String>("TAG")
            .into_iter()
            .flatten()
            .cloned();
        let access_level = matches
            .get_one::<String>("ACCESS_LEVEL")
            .map(|access_level| {
                match access_level.as_str() {
                    "not_protected" => RunnerAccessLevel::NotProtected,
                    "ref_protected" => RunnerAccessLevel::RefProtected,
                    _ => {
                        unreachable!(
                            "the `ACCESS_LEVEL` should not have let {} pass",
                            access_level,
                        )
                    },
                }
            });
        let maximum_timeout = matches.get_one::<u64>("MAXIMUM_TIMEOUT");
        let note = matches.get_one::<String>("MAINTENANCE_NOTE").cloned();

        if matches.get_flag("DRY_RUN") {
            let mut tags = tags;
            let tag_str = tags.join(",");
            let runner_type_str = match runner_type {
                RunnerType::Instance => "instance",
                RunnerType::Group => "group",
                RunnerType::Project => "project",
                _ => "<unknown>",
            };
            let id_str = id.map(|id| format!(" ({id})")).unwrap_or_else(String::new);
            println!(
                "\
                Registering {description}:\n\
                Type: {runner_type_str}{id_str}\n\
                Tags: {tag_str}\n\
                Paused: {paused}\n\
                Locked: {locked}\
                ",
            );
            if let Some(access_level) = access_level {
                println!("Access Level: {access_level:?}");
            }
            if let Some(timeout) = maximum_timeout {
                println!("Timeout: {timeout}");
            }
            if let Some(note) = note {
                println!("Maintenance note: {note}");
            }
        } else {
            let builder = move |builder: &mut CreateRunnerBuilder| {
                builder
                    .description(description)
                    .paused(paused)
                    .locked(locked)
                    .tags(tags);

                match runner_type {
                    RunnerType::Instance => builder.instance(),
                    RunnerType::Group => builder.group(*id.expect("group requires an id")),
                    RunnerType::Project => builder.group(*id.expect("project requires an id")),
                    runner_type => panic!("unknown runner type: {:?}", runner_type),
                };

                if let Some(access_level) = access_level {
                    builder.access_level(access_level);
                }

                if let Some(timeout) = maximum_timeout {
                    builder.maximum_timeout(*timeout);
                }

                if let Some(note) = note {
                    builder.maintenance_note(note);
                }
            };

            let (runner, token) = Runner::register(client, builder)
                .await
                .map_err(RegisterError::register)?;
            let id = runner.id;
            let token = token.token;
            println!(
                "\
                ID: {id}\n\
                Token: {token}\
                ",
            );
        }

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        Command::new("register")
            .about("register a runner")
            .arg(
                Arg::new("DRY_RUN")
                    .short('n')
                    .long("dry-run")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("RUNNER_TYPE")
                    .short('T')
                    .long("runner-type")
                    .help("Type of runner")
                    .required(true)
                    .value_parser(["instance", "group", "project"])
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("ID")
                    .short('i')
                    .long("id")
                    .help("ID of the group or project")
                    .value_parser(0..)
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("DESCRIPTION")
                    .short('d')
                    .long("description")
                    .help("Description for the runner")
                    .required(true)
                    .value_name("DESCRIPTION")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("PAUSED")
                    .short('p')
                    .long("paused")
                    .help("Place the runner into a paused state")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("LOCKED")
                    .short('l')
                    .long("locked")
                    .help("Lock the runner to the current set of projects")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("TAG")
                    .short('t')
                    .long("tag")
                    .help("Tag for the runner")
                    .required(true)
                    .value_name("TAG")
                    .value_delimiter(',')
                    .action(ArgAction::Append),
            )
            .arg(
                Arg::new("ACCESS_LEVEL")
                    .short('a')
                    .long("access-level")
                    .help("Access level")
                    .value_parser(["not_protected", "ref_protected"])
                    .value_name("ACCESS_LEVEL")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("MAXIMUM_TIMEOUT")
                    .short('m')
                    .long("maximum-timeout")
                    .help("Maximum timeout (in seconds)")
                    .value_parser(0..)
                    .value_name("SECONDS")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("MAINTENANCE_NOTE")
                    .short('N')
                    .long("maintenance-note")
                    .visible_alias("note")
                    .help("Maintenance note for the runner (max 1024 bytes)")
                    .value_name("NOTE")
                    .action(ArgAction::Set),
            )
    }
}
