// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::ArgMatches;
use futures_util::stream;
use futures_util::{StreamExt, TryStreamExt};
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::{Runner, RunnerDetails};
use thiserror::Error;

use crate::actions::filters;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum FetchError {
    #[error("failed to fetch runners: {}", source)]
    Runners {
        #[source]
        source: ApiError<RestError>,
    },
    #[error("failed to fetch runner details for {}: {}", id, source)]
    RunnerDetails {
        id: u64,
        #[source]
        source: ApiError<RestError>,
    },
}

impl FetchError {
    fn runners(source: ApiError<RestError>) -> Self {
        Self::Runners {
            source,
        }
    }

    fn runner_details(id: u64, source: ApiError<RestError>) -> Self {
        Self::RunnerDetails {
            id,
            source,
        }
    }
}

async fn runner_details(
    runner: Runner<AsyncGitlab>,
) -> Result<(Runner<AsyncGitlab>, RunnerDetails), (u64, ApiError<RestError>)> {
    let details = runner.details().await.map_err(|err| (runner.id, err))?;
    Ok((runner, details))
}

pub async fn fetch_runners(
    client: &AsyncGitlab,
    is_admin: bool,
    matches: &ArgMatches,
    force_details: bool,
) -> Result<Vec<(Runner<AsyncGitlab>, RunnerDetails)>, FetchError> {
    let mut runners = if is_admin {
        Runner::list_all(client).try_collect::<Vec<_>>().await
    } else {
        Runner::list(client).try_collect::<Vec<_>>().await
    }
    .map_err(FetchError::runners)?;

    let filters = filters::FilterOptions::build(matches);

    // Pre-filter runners that we don't need to query details for.
    runners.retain(|(_, overview)| filters.pre_matches(overview));

    let mut runners: Vec<_> = if force_details || filters.need_details() {
        stream::iter(runners.into_iter().map(|(runner, _)| runner))
            .then(runner_details)
            .try_collect()
            .await
            .map_err(|(id, err)| FetchError::runner_details(id, err))?
    } else {
        runners
            .into_iter()
            .map(|(runner, overview)| (runner, overview.upgrade()))
            .collect()
    };

    runners.retain(|(_, details)| filters.matches(details));

    Ok(runners)
}
