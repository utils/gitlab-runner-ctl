// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::fmt::Display;

use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::{Cell, Row};
use gitlab_runner_api::ProjectAccessTokenDetails;
use itertools::Itertools;
use thiserror::Error;

mod display;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Column {
    Id,
    Name,
    Revoked,
    CreatedAt,
    Scopes,
    UserId,
    LastUsedAt,
    Active,
    ExpiresAt,
}

impl Column {
    fn opt_display_or_default<'a, D>(opt: &'a Option<D>, dflt: &'static str) -> Cow<'a, str>
    where
        D: Display,
    {
        if let Some(val) = opt.as_ref() {
            val.to_string().into()
        } else {
            dflt.into()
        }
    }

    fn cell_for(self, details: &ProjectAccessTokenDetails) -> Cell {
        match self {
            Self::Id => Cell::new(details.id),
            Self::Name => Cell::new(&details.name),
            Self::Revoked => Cell::new(display::BoolMarkup::new(details.revoked)),
            Self::CreatedAt => Cell::new(details.created_at),
            Self::Scopes => Cell::new(details.scopes.iter().map(|s| s.as_str()).join(",")),
            Self::UserId => Cell::new(details.user_id), // TODO: get the username
            Self::LastUsedAt => {
                Cell::new(Self::opt_display_or_default(
                    &details.last_used_at,
                    "<never>",
                ))
            },
            Self::Active => Cell::new(display::BoolMarkup::new(details.active)),
            Self::ExpiresAt => {
                Cell::new(Self::opt_display_or_default(&details.expires_at, "<never>"))
            },
        }
    }

    fn header(self) -> &'static str {
        match self {
            Self::Id => "ID",
            Self::Name => "Name",
            Self::Revoked => "Revoked",
            Self::CreatedAt => "Created",
            Self::Scopes => "Scopes",
            Self::UserId => "User ID",
            Self::LastUsedAt => "Last Used",
            Self::Active => "Active",
            Self::ExpiresAt => "Expires",
        }
    }

    fn name(self) -> &'static str {
        match self {
            Self::Id => "id",
            Self::Name => "name",
            Self::Revoked => "revoked",
            Self::CreatedAt => "created_at",
            Self::Scopes => "scopes",
            Self::UserId => "user_id",
            Self::LastUsedAt => "last_used_at",
            Self::Active => "active",
            Self::ExpiresAt => "expires_at",
        }
    }
}

#[derive(Debug, Clone)]
pub struct Columns {
    columns: Vec<Column>,
}

impl Columns {
    pub fn headers(&self) -> Row {
        self.columns
            .iter()
            .map(|col| col.header())
            .collect::<Vec<_>>()
            .into()
    }

    pub fn row_for(&self, overview: &ProjectAccessTokenDetails) -> Row {
        let mut row = Row::new();

        self.columns.iter().for_each(|col| {
            row.add_cell(col.cell_for(overview));
        });

        row
    }
}

impl Default for Columns {
    fn default() -> Self {
        Self {
            columns: vec![
                Column::Id,
                Column::Name,
                Column::UserId,
                Column::ExpiresAt,
                Column::Active,
                Column::Revoked,
                Column::Scopes,
            ],
        }
    }
}

pub struct ColumnOptions;

#[derive(Debug, Error)]
enum ColumnParseError {
    #[error("unrecognized column: {}", column)]
    UnrecognizedColumn { column: String },
}

impl ColumnParseError {
    fn unrecognized_column(column: &str) -> Self {
        Self::UnrecognizedColumn {
            column: column.into(),
        }
    }
}

macro_rules! column_pairs{
    [$($col:expr,)*] => {
        [$(($col, $col.name()),)*]
    };
}

impl ColumnOptions {
    fn column_parser(arg: &str) -> Result<Columns, ColumnParseError> {
        let all_columns = column_pairs![
            Column::Id,
            Column::Name,
            Column::Revoked,
            Column::CreatedAt,
            Column::Scopes,
            Column::UserId,
            Column::LastUsedAt,
            Column::Active,
            Column::ExpiresAt,
        ];

        let columns = arg
            .split(',')
            .map(|component| {
                let (column, _) = all_columns
                    .iter()
                    .find(|(_, name)| *name == component)
                    .ok_or_else(|| ColumnParseError::unrecognized_column(component))?;

                Ok(*column)
            })
            .collect::<Result<Vec<_>, _>>()?;

        // TODO: find and reject duplicate columns

        Ok(Columns {
            columns,
        })
    }

    /// Add filter options to a command.
    pub fn add_options(cmd: Command) -> Command {
        cmd.arg(
            Arg::new("COLUMNS")
                .short('C')
                .long("columns")
                .value_parser(Self::column_parser)
                .help("Column expression")
                .value_name("COLUMNS")
                .action(ArgAction::Set),
        )
    }

    /// Build all provided filter options into a filter.
    pub fn build(matches: &ArgMatches) -> Columns {
        matches
            .get_one("COLUMNS")
            .cloned()
            .unwrap_or_else(Columns::default)
    }
}
