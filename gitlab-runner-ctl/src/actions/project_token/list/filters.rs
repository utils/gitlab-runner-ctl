// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, NaiveDate, Utc};
use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::ProjectAccessTokenDetails;
use serde::de::value::StrDeserializer;
use serde::Deserialize;
use thiserror::Error;

/// Filters to apply to runner information.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum Filter {
    /// Runners with id.
    Id(u64),
    /// Tokens created before a time.
    CreatedAfter(DateTime<Utc>),
    /// Tokens created after a time.
    CreatedBefore(DateTime<Utc>),
    /// Tokens last used before a time.
    LastUsedAfter(DateTime<Utc>),
    /// Tokens last used after a time.
    LastUsedBefore(DateTime<Utc>),
    /// Tokens that have been used.
    Used,
    /// Tokens that are revoked or not.
    Revoked,
    /// Tokens with names containing with a string.
    NameContains(String),
    /// Tokens with names starting with a string.
    NameStartsWith(String),
    /// Tokens with names matching a name.
    Named(String),
    /// Tokens which are active.
    Active,
    /// Tokens owned by a user ID.
    UserId(u64),
    /// Tokens with or without an expiration date.
    Expires,

    // Filter modifiers.
    /// Filter which must match all component filters.
    AllOf(Vec<Filter>),
    /// Filter which must match any of the component filters.
    AnyOf(Vec<Filter>),
    /// Invert a filter.
    Not(Box<Filter>),
}

impl Filter {
    /// Invert a filter.
    pub fn not(filter: Self) -> Self {
        Self::Not(Box::new(filter))
    }

    /// Check whether a filter matches a given token's details.
    pub fn matches(&self, details: &ProjectAccessTokenDetails) -> bool {
        match self {
            Self::Id(id) => details.id == *id,
            Self::CreatedAfter(dt) => *dt < details.created_at,
            Self::CreatedBefore(dt) => details.created_at < *dt,
            Self::LastUsedAfter(dt) => details.last_used_at.map(|lu| *dt < lu).unwrap_or(false),
            Self::LastUsedBefore(dt) => details.last_used_at.map(|lu| lu < *dt).unwrap_or(false),
            Self::Used => details.last_used_at.is_some(),
            Self::Revoked => details.revoked,
            Self::NameContains(n) => details.name.contains(n),
            Self::NameStartsWith(n) => details.name.starts_with(n),
            Self::Named(n) => details.name == *n,
            Self::Active => details.active,
            Self::UserId(u) => details.user_id == *u,
            Self::Expires => details.expires_at.is_some(),

            Self::AllOf(filters) => filters.iter().all(|f| f.matches(details)),
            Self::AnyOf(filters) => filters.iter().any(|f| f.matches(details)),
            Self::Not(filter) => !filter.matches(details),
        }
    }
}

pub struct FilterOptions;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
enum FilterParseError {
    #[error("invalid date time: {}", dt)]
    InvalidDateTime { dt: String },
    #[error("unrecognized filter: {}", filter)]
    UnrecognizedFilter { filter: String },
}

impl FilterParseError {
    fn invalid_date_time(dt: &str) -> Self {
        Self::InvalidDateTime {
            dt: dt.into(),
        }
    }

    fn unrecognized_filter(filter: &str) -> Self {
        Self::UnrecognizedFilter {
            filter: filter.into(),
        }
    }
}

#[derive(Debug, Error)]
#[error("date time parse error: {}", msg)]
struct DateTimeParserError {
    msg: String,
}

impl serde::de::Error for DateTimeParserError {
    fn custom<T>(msg: T) -> Self
    where
        T: std::fmt::Display,
    {
        Self {
            msg: msg.to_string(),
        }
    }
}

fn parse_datetime(dt: &str) -> Result<DateTime<Utc>, FilterParseError> {
    let deser = StrDeserializer::<DateTimeParserError>::new(dt);

    if let Ok(d) = NaiveDate::deserialize(deser) {
        Ok(d.and_hms_opt(0, 0, 0)
            .expect("00:00:00 is a valid time")
            .and_utc())
    } else if let Ok(dt) = DateTime::<Utc>::deserialize(deser) {
        Ok(dt)
    } else {
        Err(FilterParseError::invalid_date_time(dt))
    }
}

impl FilterOptions {
    fn filter_parser(arg: &str) -> Result<Filter, FilterParseError> {
        let filters = arg
            .split(',')
            .map(|component| {
                let (invert, component) = if let Some(rest) = component.strip_prefix('!') {
                    (true, rest)
                } else {
                    (false, component)
                };

                let filter = if let Some(id) = component.strip_prefix("id=") {
                    Filter::Id(id.parse::<u64>().expect("Id should be in integer"))
                } else if let Some(name) = component.strip_prefix("name=") {
                    if let Some(prefix) = name.strip_suffix('*') {
                        if let Some(internal) = prefix.strip_prefix('*') {
                            Filter::NameContains(internal.into())
                        } else {
                            Filter::NameStartsWith(prefix.into())
                        }
                    } else {
                        Filter::Named(name.into())
                    }
                } else if let Some(dt) = component.strip_prefix("created<") {
                    Filter::CreatedBefore(parse_datetime(dt)?)
                } else if let Some(dt) = component.strip_prefix("created>") {
                    Filter::CreatedAfter(parse_datetime(dt)?)
                } else if let Some(dt) = component.strip_prefix("last_used<") {
                    Filter::LastUsedBefore(parse_datetime(dt)?)
                } else if let Some(dt) = component.strip_prefix("last_used>") {
                    Filter::LastUsedAfter(parse_datetime(dt)?)
                } else if let Some(user_id) = component.strip_prefix("user_id=") {
                    Filter::UserId(user_id.parse::<u64>().expect("UserId should be in integer"))
                } else if component == "active" {
                    Filter::Active
                } else if component == "revoked" {
                    Filter::Revoked
                } else if component == "expires" {
                    Filter::Expires
                } else if component == "used" {
                    Filter::Used
                } else {
                    return Err(FilterParseError::unrecognized_filter(component));
                };

                Ok(if invert { Filter::not(filter) } else { filter })
            })
            .collect::<Result<Vec<_>, _>>()?;

        Ok(Filter::AnyOf(filters))
    }

    /// Add filter options to a command.
    pub fn add_options(cmd: Command) -> Command {
        cmd.arg(
            Arg::new("FILTER")
                .short('F')
                .long("filter")
                .value_parser(Self::filter_parser)
                .help("Filter expression")
                .value_name("FILTER")
                .action(ArgAction::Append),
        )
    }

    /// Build all provided filter options into a filter.
    pub fn build(matches: &ArgMatches) -> Filter {
        let filters = if let Some(matches) = matches.get_many("FILTER") {
            matches.into_iter().cloned().collect()
        } else {
            Vec::new()
        };

        Filter::AllOf(filters)
    }
}
