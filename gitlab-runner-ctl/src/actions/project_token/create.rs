// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::NaiveDate;
use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::gitlab::api::common::NameOrId;
use gitlab_runner_api::gitlab::api::{self, ApiError};
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::{
    ProjectAccessToken, ProjectAccessTokenParameters, ProjectAccessTokenScope,
};
use itertools::Itertools;
use thiserror::Error;

use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum ProjectTokenCreateError {
    #[error("failed to create token: {}", source)]
    CreateToken {
        #[source]
        source: ApiError<RestError>,
    },
}

impl ProjectTokenCreateError {
    fn create_token(source: ApiError<RestError>) -> Self {
        Self::CreateToken {
            source,
        }
    }
}

type ProjectTokenCreateResult<T> = Result<T, ProjectTokenCreateError>;

pub struct ProjectTokenCreate;

#[derive(Debug, Error)]
enum ScopeParserError {
    #[error("invalid scope: {}", scope)]
    InvalidScope { scope: String },
}

impl ScopeParserError {
    fn invalid_scope(scope: &str) -> Self {
        Self::InvalidScope {
            scope: scope.into(),
        }
    }
}

fn parse_scope(arg: &str) -> Result<ProjectAccessTokenScope, ScopeParserError> {
    match arg {
        "api" => Ok(ProjectAccessTokenScope::Api),
        "read_api" => Ok(ProjectAccessTokenScope::ReadApi),
        "read_repository" => Ok(ProjectAccessTokenScope::ReadRepository),
        "write_repository" => Ok(ProjectAccessTokenScope::WriteRepository),
        "read_registry" => Ok(ProjectAccessTokenScope::ReadRegistry),
        "write_registry" => Ok(ProjectAccessTokenScope::WriteRegistry),
        "create_runner" => Ok(ProjectAccessTokenScope::CreateRunner),
        "ai_features" => Ok(ProjectAccessTokenScope::AiFeatures),
        "k8s_proxy" => Ok(ProjectAccessTokenScope::K8sProxy),
        _ => Err(ScopeParserError::invalid_scope(arg)),
    }
}

fn scope_api(
    scope: &ProjectAccessTokenScope,
) -> Option<api::projects::access_tokens::ProjectAccessTokenScope> {
    match scope {
        ProjectAccessTokenScope::Api => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::Api)
        },
        ProjectAccessTokenScope::ReadApi => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::ReadApi)
        },
        ProjectAccessTokenScope::ReadRepository => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::ReadRepository)
        },
        ProjectAccessTokenScope::WriteRepository => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::WriteRepository)
        },
        ProjectAccessTokenScope::ReadRegistry => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::ReadRegistry)
        },
        ProjectAccessTokenScope::WriteRegistry => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::WriteRegistry)
        },
        ProjectAccessTokenScope::CreateRunner => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::CreateRunner)
        },
        ProjectAccessTokenScope::AiFeatures => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::AiFeatures)
        },
        ProjectAccessTokenScope::K8sProxy => {
            Some(api::projects::access_tokens::ProjectAccessTokenScope::K8sProxy)
        },
    }
}

impl ProjectTokenCreate {
    pub async fn run(
        client: AsyncGitlab,
        project: NameOrId<'_>,
        matches: &ArgMatches,
    ) -> ProjectTokenCreateResult<ExitCode> {
        let mut builder = ProjectAccessTokenParameters::builder();
        builder
            .name(
                matches
                    .get_one::<String>("NAME")
                    .expect("NAME is a required argument"),
            )
            .scopes(
                matches
                    .get_many::<ProjectAccessTokenScope>("SCOPE")
                    .expect("SCOPE is a required argument")
                    .filter_map(scope_api),
            );
        if let Some(expires_at) = matches.get_one::<NaiveDate>("EXPIRATION") {
            builder.expires_at(*expires_at);
        }
        let params = builder.build().unwrap();

        let (_, details) = ProjectAccessToken::create(client, project, params)
            .await
            .map_err(ProjectTokenCreateError::create_token)?;

        println!("ID: {}", details.details.id);
        println!("Name: {}", details.details.name);
        println!(
            "Expires: {}",
            details
                .details
                .expires_at
                .map(|e| e.to_string())
                .unwrap_or_else(|| "<never>".into()),
        );
        println!(
            "Scopes: {}",
            details.details.scopes.iter().map(|s| s.as_str()).join(", "),
        );
        println!(
            "Scopes: {}",
            details.details.scopes.iter().map(|s| s.as_str()).join(", "),
        );
        println!("Token: {}", details.token);

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        Command::new("create")
            .about("create an API token for a project")
            .arg(
                Arg::new("NAME")
                    .short('n')
                    .long("name")
                    .help("Name of the token")
                    .value_name("NAME")
                    .required(true)
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("EXPIRATION")
                    .short('e')
                    .long("expiration")
                    .help("Expiration date")
                    .value_parser(clap::value_parser!(NaiveDate))
                    .value_name("EXPIRATION")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("SCOPE")
                    .short('s')
                    .long("scope")
                    .help("Add a scope to the token")
                    .value_parser(parse_scope)
                    .value_name("SCOPE")
                    .required(true)
                    .action(ArgAction::Append),
            )
    }
}
