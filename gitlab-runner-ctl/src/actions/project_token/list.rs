// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{ArgMatches, Command};
use comfy_table::Table;
use futures_util::TryStreamExt;
use gitlab_runner_api::gitlab::api::common::NameOrId;
use gitlab_runner_api::gitlab::api::ApiError;
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use gitlab_runner_api::ProjectAccessToken;
use thiserror::Error;

use crate::exit_code::ExitCode;

mod columns;
mod filters;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum ProjectTokenListError {
    #[error("failed to fetch tokens: {}", source)]
    Fetch {
        #[from]
        source: ApiError<RestError>,
    },
}

type ProjectTokenListResult<T> = Result<T, ProjectTokenListError>;

pub struct ProjectTokenList;

impl ProjectTokenList {
    pub async fn run(
        client: AsyncGitlab,
        project: NameOrId<'_>,
        matches: &ArgMatches,
    ) -> ProjectTokenListResult<ExitCode> {
        let columns = columns::ColumnOptions::build(matches);
        let filters = filters::FilterOptions::build(matches);

        let tokens = {
            let mut tokens = ProjectAccessToken::list(&client, project)
                .try_collect::<Vec<_>>()
                .await?;
            tokens.retain(|(_, details)| filters.matches(details));
            tokens
        };

        // TODO: sorting

        let mut table = Table::new();
        // TODO: apply formatting

        table.set_header(columns.headers());
        for (_, details) in tokens {
            table.add_row(columns.row_for(&details));
        }
        println!("{}", table);

        Ok(ExitCode::Success)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("list").about("list personal access tokens");
        let cmd = filters::FilterOptions::add_options(cmd);
        columns::ColumnOptions::add_options(cmd)
    }
}
