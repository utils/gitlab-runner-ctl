// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::gitlab::api::common::NameOrId;
use gitlab_runner_api::gitlab::api::{self, ApiError, AsyncQuery};
use gitlab_runner_api::gitlab::{AsyncGitlab, RestError};
use serde::Deserialize;
use thiserror::Error;

use crate::exit_code::ExitCode;

mod create;
mod list;
mod revoke;
mod rotate;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum ProjectTokenError {
    #[error("failed to find project '{}': {}", project, source)]
    NoSuchProject {
        project: NameOrId<'static>,
        #[source]
        source: ApiError<RestError>,
    },
    #[error("unknown command '{}'", command)]
    UnknownCommand { command: String },
    #[error("`create` error: {}", source)]
    Create {
        #[from]
        source: self::create::ProjectTokenCreateError,
    },
    #[error("`list` error: {}", source)]
    List {
        #[from]
        source: self::list::ProjectTokenListError,
    },
    #[error("`revoke` error: {}", source)]
    Revoke {
        #[from]
        source: self::revoke::ProjectTokenRevokeError,
    },
    #[error("`rotate` error: {}", source)]
    Rotate {
        #[from]
        source: self::rotate::ProjectTokenRotateError,
    },
}

impl ProjectTokenError {
    fn no_such_project(project: NameOrId, source: ApiError<RestError>) -> Self {
        Self::NoSuchProject {
            project: match project {
                NameOrId::Name(n) => n.into_owned().into(),
                NameOrId::Id(i) => i.into(),
            },
            source,
        }
    }

    fn unknown_command(command: String) -> Self {
        Self::UnknownCommand {
            command,
        }
    }
}

type ProjectTokenResult<T> = Result<T, ProjectTokenError>;

pub struct ProjectToken;

#[derive(Debug, Deserialize)]
struct Project {
    id: u64,
}

fn as_name_or_id(input: &str) -> NameOrId {
    if let Ok(id) = input.parse::<u64>() {
        id.into()
    } else {
        input.into()
    }
}

impl ProjectToken {
    pub async fn run(client: AsyncGitlab, matches: &ArgMatches) -> ProjectTokenResult<ExitCode> {
        let project = as_name_or_id(
            matches
                .get_one::<String>("PROJECT")
                .expect("`PROJECT` is required by the option parser"),
        );
        let project = {
            let endpoint = api::projects::Project::builder()
                .project(project.clone())
                .build()
                .unwrap();
            let data: Project = endpoint
                .query_async(&client)
                .await
                .map_err(|err| ProjectTokenError::no_such_project(project, err))?;
            data.id.into()
        };

        let status = match matches.subcommand() {
            Some(("create", m)) => {
                self::create::ProjectTokenCreate::run(client, project, m).await?
            },
            Some(("list", m)) => self::list::ProjectTokenList::run(client, project, m).await?,
            Some(("revoke", m)) => {
                self::revoke::ProjectTokenRevoke::run(client, project, m).await?
            },
            Some(("rotate", m)) => {
                self::rotate::ProjectTokenRotate::run(client, project, m).await?
            },
            Some((subcmd, _)) => return Err(ProjectTokenError::unknown_command(subcmd.into())),
            None => ExitCode::Success,
        };

        Ok(status)
    }

    pub fn subcommand() -> Command {
        Command::new("project-token")
            .about("manage API tokens for projects")
            .arg(
                Arg::new("PROJECT")
                    .short('p')
                    .long("project")
                    .help("Project name or ID")
                    .value_name("PROJECT")
                    .action(ArgAction::Set),
            )
            .subcommand(self::create::ProjectTokenCreate::subcommand())
            .subcommand(self::list::ProjectTokenList::subcommand())
            .subcommand(self::revoke::ProjectTokenRevoke::subcommand())
            .subcommand(self::rotate::ProjectTokenRotate::subcommand())
    }
}
