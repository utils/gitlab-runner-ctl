// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::actions;
use gitlab_runner_api::gitlab::api::runners::EditRunnerBuilder;
use gitlab_runner_api::gitlab::AsyncGitlab;
use thiserror::Error;

use crate::actions::{filters, helpers};
use crate::exit_code::ExitCode;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum UnpauseError {
    #[error("failed to fetch runners: {}", source)]
    Fetch {
        #[from]
        source: helpers::FetchError,
    },
}

type UnpauseResult<T> = Result<T, UnpauseError>;

pub struct Unpause;

impl Unpause {
    pub async fn run(
        client: AsyncGitlab,
        is_admin: bool,
        matches: &ArgMatches,
    ) -> UnpauseResult<ExitCode> {
        let runners = helpers::fetch_runners(&client, is_admin, matches, false).await?;
        let clear_maint_note = matches.get_flag("CLEAR_MAINTENANCE_NOTE");
        let unpause_action = |builder: &mut EditRunnerBuilder| {
            actions::unpause(builder);
            if clear_maint_note {
                builder.maintenance_note("");
            }
        };

        let mut code = ExitCode::Success;
        for (runner, overview) in runners {
            let label = overview
                .description
                .as_ref()
                .or(overview.name.as_ref())
                .map(AsRef::as_ref)
                .unwrap_or("<unknown>");

            if matches.get_flag("DRY_RUN") {
                if overview.paused {
                    println!("Runner #{} ({}) is already unpaused", overview.id, label);
                } else {
                    println!("Runner #{} ({}) would be unpaused", overview.id, label);
                }
                continue;
            }

            match runner.update(unpause_action).await {
                Ok(_) => {
                    println!("Runner #{} ({}) has been unpaused", overview.id, label);
                },
                Err(err) => {
                    log::error!(
                        "Failed to unpause runner #{} ({}): {:?}",
                        overview.id,
                        label,
                        err,
                    );
                    code = ExitCode::Failure;
                },
            }
        }

        Ok(code)
    }

    pub fn subcommand() -> Command {
        let cmd = Command::new("unpause")
            .about("unpause runners on an instance")
            .arg(
                Arg::new("CLEAR_MAINTENANCE_NOTE")
                    .short('c')
                    .long("clear-maintenance-note")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("DRY_RUN")
                    .short('n')
                    .long("dry-run")
                    .action(ArgAction::SetTrue),
            );
        filters::FilterOptions::add_options(cmd)
    }
}
