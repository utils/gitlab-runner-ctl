// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use gitlab_runner_api::{RunnerAccessLevel, RunnerDetails, RunnerOverview, RunnerType};
use thiserror::Error;

/// Filters to apply to runner information.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum Filter {
    /// Runners with id.
    Id(u64),
    /// Runners without a name.
    Unnamed,
    /// Runners whose name is the given string.
    Named(String),
    /// Runners whose name start with a string.
    NameStartsWith(String),
    /// Runners whose name contains a string.
    NameContains(String),
    /// Runners without a description.
    Undescribed,
    /// Runners whose description is the given string.
    Described(String),
    /// Runners whose description start with a string.
    DescriptionStartsWith(String),
    /// Runners whose description contains a string.
    DescriptionContains(String),
    /// Runners of a given type.
    Type(RunnerType),
    /// Runners with a tag.
    WithTag(String),
    /// Runners with a given access level.
    AccessLevel(RunnerAccessLevel),
    /// Runners which are online.
    Online,
    /// Runners which are paused.
    Paused,
    /// Runners with maintenance notes.
    Noted,
    /// Runners with a specific maintenance note.
    NotedAs(String),

    // Filter modifiers.
    /// Filter which must match all component filters.
    AllOf(Vec<Filter>),
    /// Filter which must match any of the component filters.
    AnyOf(Vec<Filter>),
    /// Invert a filter.
    Not(Box<Filter>),
}

#[derive(Debug, Clone, Copy)]
pub enum PreMatch {
    Match,
    Indeterminite,
    NotMatch,
}

impl PreMatch {
    fn is_not_match(self) -> bool {
        matches!(self, Self::NotMatch)
    }

    fn invert(self) -> Self {
        match self {
            Self::Match => Self::NotMatch,
            Self::NotMatch => Self::Match,
            Self::Indeterminite => Self::Indeterminite,
        }
    }

    fn and(self, rhs: Self) -> Self {
        match (self, rhs) {
            (Self::Indeterminite, _) | (_, Self::Indeterminite) => Self::Indeterminite,
            (Self::Match, Self::Match) => Self::Match,
            (Self::NotMatch, _) | (_, Self::NotMatch) => Self::NotMatch,
        }
    }

    fn or(self, rhs: Self) -> Self {
        match (self, rhs) {
            (Self::Indeterminite, _) | (_, Self::Indeterminite) => Self::Indeterminite,
            (Self::NotMatch, Self::NotMatch) => Self::NotMatch,
            (Self::Match, _) | (_, Self::Match) => Self::Match,
        }
    }
}

impl From<bool> for PreMatch {
    fn from(b: bool) -> Self {
        if b {
            Self::Match
        } else {
            Self::NotMatch
        }
    }
}

impl Filter {
    pub fn need_details(&self) -> bool {
        match self {
            Self::Id(_)
            | Self::Unnamed
            | Self::Named(_)
            | Self::NameStartsWith(_)
            | Self::NameContains(_)
            | Self::Undescribed
            | Self::Described(_)
            | Self::DescriptionStartsWith(_)
            | Self::DescriptionContains(_)
            | Self::Type(_)
            | Self::Online
            | Self::Paused => false,
            Self::WithTag(_) | Self::AccessLevel(_) | Self::Noted | Self::NotedAs(_) => true,
            Self::AllOf(filters) | Self::AnyOf(filters) => filters.iter().any(Self::need_details),
            Self::Not(filter) => filter.need_details(),
        }
    }

    /// Invert a filter.
    pub fn not(filter: Self) -> Self {
        Self::Not(Box::new(filter))
    }

    /// Check whether a filter matches a given runner's details.
    pub fn pre_matches(&self, overview: &RunnerOverview) -> bool {
        !self.pre_matches_impl(overview).is_not_match()
    }

    /// Check whether a filter matches a given runner's details.
    fn pre_matches_impl(&self, overview: &RunnerOverview) -> PreMatch {
        match self {
            Self::Id(id) => (overview.id == *id).into(),
            Self::Unnamed => overview.name.is_none().into(),
            Self::Named(name) => {
                overview
                    .name
                    .as_ref()
                    .map(|n| n == name)
                    .unwrap_or(false)
                    .into()
            },
            Self::NameStartsWith(name) => {
                overview
                    .name
                    .as_ref()
                    .map(|n| n.starts_with(name))
                    .unwrap_or(false)
                    .into()
            },
            Self::NameContains(name) => {
                overview
                    .name
                    .as_ref()
                    .map(|n| n.contains(name))
                    .unwrap_or(false)
                    .into()
            },
            Self::Undescribed => overview.description.is_none().into(),
            Self::Described(descr) => {
                overview
                    .description
                    .as_ref()
                    .map(|n| n == descr)
                    .unwrap_or(false)
                    .into()
            },
            Self::DescriptionStartsWith(descr) => {
                overview
                    .description
                    .as_ref()
                    .map(|d| d.starts_with(descr))
                    .unwrap_or(false)
                    .into()
            },
            Self::DescriptionContains(descr) => {
                overview
                    .description
                    .as_ref()
                    .map(|d| d.contains(descr))
                    .unwrap_or(false)
                    .into()
            },
            Self::Type(type_) => (overview.runner_type == *type_).into(),
            Self::WithTag(_) => PreMatch::Indeterminite,
            Self::AccessLevel(_) => PreMatch::Indeterminite,
            Self::Online => overview.online.unwrap_or(false).into(),
            Self::Paused => overview.paused.into(),
            Self::Noted => PreMatch::Indeterminite,
            Self::NotedAs(_) => PreMatch::Indeterminite,

            Self::AllOf(filters) => {
                filters
                    .iter()
                    .fold(PreMatch::Match, |m, f| m.and(f.pre_matches_impl(overview)))
            },
            Self::AnyOf(filters) => {
                filters.iter().fold(PreMatch::NotMatch, |m, f| {
                    m.or(f.pre_matches_impl(overview))
                })
            },
            Self::Not(filter) => filter.pre_matches_impl(overview).invert(),
        }
    }

    /// Check whether a filter matches a given runner's details.
    pub fn matches(&self, details: &RunnerDetails) -> bool {
        match self {
            Self::Id(id) => details.id == *id,
            Self::Unnamed => details.name.is_none(),
            Self::Named(name) => details.name.as_ref().map(|n| n == name).unwrap_or(false),
            Self::NameStartsWith(name) => {
                details
                    .name
                    .as_ref()
                    .map(|n| n.starts_with(name))
                    .unwrap_or(false)
            },
            Self::NameContains(name) => {
                details
                    .name
                    .as_ref()
                    .map(|n| n.contains(name))
                    .unwrap_or(false)
            },
            Self::Undescribed => details.description.is_none(),
            Self::Described(descr) => {
                details
                    .description
                    .as_ref()
                    .map(|n| n == descr)
                    .unwrap_or(false)
            },
            Self::DescriptionStartsWith(descr) => {
                details
                    .description
                    .as_ref()
                    .map(|d| d.starts_with(descr))
                    .unwrap_or(false)
            },
            Self::DescriptionContains(descr) => {
                details
                    .description
                    .as_ref()
                    .map(|d| d.contains(descr))
                    .unwrap_or(false)
            },
            Self::Type(type_) => details.runner_type == *type_,
            Self::WithTag(tag) => details.tags.contains(tag),
            Self::AccessLevel(access) => details.access_level == *access,
            Self::Online => details.online.unwrap_or(false),
            Self::Paused => details.paused,
            Self::Noted => {
                details
                    .maintenance_note
                    .as_ref()
                    .map(|s| !s.is_empty())
                    .unwrap_or(false)
            },
            Self::NotedAs(needle) => {
                details
                    .maintenance_note
                    .as_ref()
                    .map(|s| s == needle)
                    .unwrap_or(false)
            },

            Self::AllOf(filters) => filters.iter().all(|f| f.matches(details)),
            Self::AnyOf(filters) => filters.iter().any(|f| f.matches(details)),
            Self::Not(filter) => !filter.matches(details),
        }
    }
}

pub struct FilterOptions;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
enum FilterParseError {
    #[error("unrecognized runner type: {}", type_)]
    UnrecognizedType { type_: String },
    #[error("unrecognized runner access level: {}", access)]
    UnrecognizedAccessLevel { access: String },
    #[error("unrecognized filter: {}", filter)]
    UnrecognizedFilter { filter: String },
}

impl FilterParseError {
    fn unrecognized_type(type_: &str) -> Self {
        Self::UnrecognizedType {
            type_: type_.into(),
        }
    }

    fn unrecognized_access_level(access: &str) -> Self {
        Self::UnrecognizedAccessLevel {
            access: access.into(),
        }
    }

    fn unrecognized_filter(filter: &str) -> Self {
        Self::UnrecognizedFilter {
            filter: filter.into(),
        }
    }
}

impl FilterOptions {
    fn filter_parser(arg: &str) -> Result<Filter, FilterParseError> {
        let filters = arg
            .split(',')
            .map(|component| {
                let (invert, component) = if let Some(rest) = component.strip_prefix('!') {
                    (true, rest)
                } else {
                    (false, component)
                };

                let filter = if let Some(id) = component.strip_prefix("id=") {
                    Filter::Id(id.parse::<u64>().expect("Id should be in integer"))
                } else if let Some(name) = component.strip_prefix("name=") {
                    if let Some(prefix) = name.strip_suffix('*') {
                        if let Some(internal) = prefix.strip_prefix('*') {
                            Filter::NameContains(internal.into())
                        } else {
                            Filter::NameStartsWith(prefix.into())
                        }
                    } else if name.is_empty() {
                        Filter::Unnamed
                    } else {
                        Filter::Named(name.into())
                    }
                } else if let Some(descr) = component.strip_prefix("description=") {
                    if let Some(prefix) = descr.strip_suffix('*') {
                        if let Some(internal) = prefix.strip_prefix('*') {
                            Filter::DescriptionContains(internal.into())
                        } else {
                            Filter::DescriptionStartsWith(prefix.into())
                        }
                    } else if descr.is_empty() {
                        Filter::Undescribed
                    } else {
                        Filter::Described(descr.into())
                    }
                } else if let Some(type_) = component.strip_prefix("type=") {
                    Filter::Type(match type_ {
                        "instance" => RunnerType::Instance,
                        "group" => RunnerType::Group,
                        "project" => RunnerType::Project,
                        _ => {
                            return Err(FilterParseError::unrecognized_type(type_));
                        },
                    })
                } else if let Some(tag) = component.strip_prefix("tag=") {
                    Filter::WithTag(tag.into())
                } else if let Some(access) = component.strip_prefix("access_level=") {
                    Filter::AccessLevel(match access {
                        "not_protected" => RunnerAccessLevel::NotProtected,
                        "ref_protected" => RunnerAccessLevel::RefProtected,
                        _ => {
                            return Err(FilterParseError::unrecognized_access_level(access));
                        },
                    })
                } else if let Some(note) = component.strip_prefix("noted_as=") {
                    Filter::NotedAs(note.into())
                } else if component == "online" {
                    Filter::Online
                } else if component == "paused" {
                    Filter::Paused
                } else if component == "noted" {
                    Filter::Noted
                } else {
                    return Err(FilterParseError::unrecognized_filter(component));
                };

                Ok(if invert { Filter::not(filter) } else { filter })
            })
            .collect::<Result<Vec<_>, _>>()?;

        Ok(Filter::AnyOf(filters))
    }

    /// Add filter options to a command.
    pub fn add_options(cmd: Command) -> Command {
        cmd.arg(
            Arg::new("FILTER")
                .short('F')
                .long("filter")
                .value_parser(Self::filter_parser)
                .help("Filter expression")
                .value_name("FILTER")
                .action(ArgAction::Append),
        )
    }

    /// Build all provided filter options into a filter.
    pub fn build(matches: &ArgMatches) -> Filter {
        let filters = if let Some(matches) = matches.get_many("FILTER") {
            matches.into_iter().cloned().collect()
        } else {
            Vec::new()
        };

        Filter::AllOf(filters)
    }
}
